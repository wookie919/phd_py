var N = 0

var curCyc = 0

var matrix = []

function generate(n) {
	N = n

	// initialize global matrix
	for (cyc = 0; cyc < 1000; cyc++) {
		matrix[cyc] = []
		for (i = 0; i < N; i++) {
			matrix[cyc][i] = []
			for (j = 0; j < N; j++) {
				matrix[cyc][i][j] = {}
				matrix[cyc][i][j]["ul"] = ""
				matrix[cyc][i][j]["uc"] = ""
				matrix[cyc][i][j]["ur"] = ""
				matrix[cyc][i][j]["ml"] = ""
				matrix[cyc][i][j]["mc"] = ""
				matrix[cyc][i][j]["mr"] = ""
				matrix[cyc][i][j]["ll"] = ""
				matrix[cyc][i][j]["lc"] = ""
				matrix[cyc][i][j]["lr"] = ""
			}
		}
	}

	curCyc = 1
	
	// Generate random distances
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			if (i != j) {
				dist = Math.floor(Math.random() * 99) + 1
			}
			else {
				dist = 0
			}
			id = i.toString() + j.toString() + "_mc"
			document.getElementById(id).innerHTML = dist
			matrix[curCyc][i][j]["mc"] = dist
		}
	}

	for (i = 0; i < N; i++) {
		id_ur = i.toString() + i.toString() + "_ur"
		id_lr = i.toString() + i.toString() + "_lr"
		document.getElementById(id_ur).innerHTML = "rd"
		document.getElementById(id_lr).innerHTML = "dr"
		matrix[curCyc][i][i]["ur"] = "rd"
		matrix[curCyc][i][i]["lr"] = "dr"
	}
	
	display(curCyc)
}


function display(cycle) {
	document.getElementById("cycle").innerHTML = cycle
	prev = cycle - 1
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			id = i.toString() + j.toString() + "_ul";
			document.getElementById(id).innerHTML = matrix[cycle][i][j]["ul"]
			id = i.toString() + j.toString() + "_uc";
			document.getElementById(id).innerHTML = matrix[cycle][i][j]["uc"]
			id = i.toString() + j.toString() + "_ur";
			document.getElementById(id).innerHTML = matrix[cycle][i][j]["ur"]
			id = i.toString() + j.toString() + "_ml";
			document.getElementById(id).innerHTML = matrix[cycle][i][j]["ml"]

			id = i.toString() + j.toString() + "_mc";
			if (matrix[prev][i][j]["mc"] != matrix[cycle][i][j]["mc"]) {
				document.getElementById(id).style.background = "green"
			}
			else {
				document.getElementById(id).style.background = ""
			}
			document.getElementById(id).innerHTML = matrix[cycle][i][j]["mc"]

			id = i.toString() + j.toString() + "_mr";
			document.getElementById(id).innerHTML = matrix[cycle][i][j]["mr"]
			id = i.toString() + j.toString() + "_ll";
			document.getElementById(id).innerHTML = matrix[cycle][i][j]["ll"]
			id = i.toString() + j.toString() + "_lc";
			document.getElementById(id).innerHTML = matrix[cycle][i][j]["lc"]
			id = i.toString() + j.toString() + "_lr";
			document.getElementById(id).innerHTML = matrix[cycle][i][j]["lr"]
		}
	}
}


function next() {
	nexCyc = curCyc + 1

	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			inext = i + 1
			jnext = j + 1
			iprev = i - 1
			jprev = j - 1

			// Process Signals
			if (matrix[curCyc][i][j]["ur"] == "rd") {
				// Move signal right
				matrix[nexCyc][i][j]["ur"] = ""
				if (jnext < N) {
					matrix[nexCyc][i][jnext]["ur"] = "rd"
				}
				// Copy data down
				if (inext < N) {
					matrix[nexCyc][inext][j]["uc"] = matrix[curCyc][i][j]["mc"]
				}
			}
			if (matrix[curCyc][i][j]["lr"] == "dr") {
				// Move signal down
				matrix[nexCyc][i][j]["lr"] = ""
				if (inext < N) {
					matrix[nexCyc][inext][j]["lr"] = "dr"
				}
				// Copy data right
				if (jnext < N) {
					matrix[nexCyc][i][jnext]["ml"] = matrix[curCyc][i][j]["mc"]
				}
			}

			// Forward Data
			if (matrix[curCyc][i][j]["uc"] > 0) {
				// forward down
				if (inext < N) {
					matrix[nexCyc][inext][j]["uc"] = matrix[curCyc][i][j]["uc"]
				}
			}
			if (matrix[curCyc][i][j]["ml"] > 0) {
				// forward right
				if (jnext < N) {
					matrix[nexCyc][i][jnext]["ml"] = matrix[curCyc][i][j]["ml"]
				}
			}
		}
	}

	// Detect Collisions
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			if (matrix[nexCyc][i][j]["uc"] > 0 && matrix[nexCyc][i][j]["ur"] == "rd") {
				console.log("!!! COLLISION DETECTED !!!")
			}
			if (matrix[nexCyc][i][j]["mr"] > 0 && matrix[nexCyc][i][j]["lr"] == "dl") {
				console.log("!!! COLLISION DETECTED !!!")
			}
			if (matrix[nexCyc][i][j]["lc"] > 0 && matrix[nexCyc][i][j]["ll"] == "lu") {
				console.log("!!! COLLISION DETECTED !!!")
			}
			if (matrix[nexCyc][i][j]["ml"] > 0 && matrix[nexCyc][i][j]["ul"] == "ur") {
				console.log("!!! COLLISION DETECTED !!!")
			}

			if (matrix[nexCyc][i][j]["uc"] > 0 && matrix[nexCyc][i][j]["ll"] == "ld") {
				console.log("!!! COLLISION DETECTED !!!")
			}
			if (matrix[nexCyc][i][j]["mr"] > 0 && matrix[nexCyc][i][j]["ul"] == "ul") {
				console.log("!!! COLLISION DETECTED !!!")
			}
			if (matrix[nexCyc][i][j]["lc"] > 0 && matrix[nexCyc][i][j]["ur"] == "ru") {
				console.log("!!! COLLISION DETECTED !!!")
			}
			if (matrix[nexCyc][i][j]["ml"] > 0 && matrix[nexCyc][i][j]["lr"] == "dr") {
				console.log("!!! COLLISION DETECTED !!!")
			}
		}
	}
	
	// Update distances
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			curDist = matrix[curCyc][i][j]["mc"]
			uc = matrix[nexCyc][i][j]["uc"]
			mr = matrix[nexCyc][i][j]["mr"]
			lc = matrix[nexCyc][i][j]["lc"]
			ml = matrix[nexCyc][i][j]["ml"]
			if (uc > 0 && mr > 0) {
				if (uc + mr < curDist) {
					curDist = uc + mr
				}
			}
			if (mr > 0 && lc > 0) {
				if (mr + lc < curDist) {
					curDist = mr + lc
				}
			}
			if (lc > 0 && ml > 0) {
				if (lc + ml < curDist) {
					curDist = lc + ml
				}
			}
			if (ml > 0 && uc > 0) {
				if (ml + uc < curDist) {
					curDist = ml + uc
				}
			}
			matrix[nexCyc][i][j]["mc"] = curDist
		}
	}
	
	display(nexCyc)
	
	curCyc = nexCyc
}


function prev() {
}
