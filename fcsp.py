#!/usr/bin/python
import sys
import os
import time
import random

DEBUG = False

class Edge:
	def __init__(self, src, dst, cap):
		self.src = src
		self.dst = dst
		self.cap = cap

class Flow:
	def __init__(self, src, dst, req):
		self.src = src
		self.dst = dst
		self.req = req


def debug(msg):
	if DEBUG:
		print(msg)


def simple(EDGES, FLOWS, NUM_NODES, NUM_EDGES, MAX_CAP, SRC):

	class Node:
		def __init__(self, id):
			self.id = id
			self.parent = None
			self.edges = []
			self.inQ = False

	class Queue: 
		def __init__(self):
			self.in_stack = []
			self.out_stack = []
		def push(self, obj):
			self.in_stack.append(obj)
		def pop(self):
			if not self.out_stack:
				while self.in_stack:
					self.out_stack.append(self.in_stack.pop())
			if not self.out_stack:
				return None
			else:
				return self.out_stack.pop()

	NODES = [Node(n) for n in range(NUM_NODES)]
	for src in range(NUM_NODES):
		for dst in range(NUM_NODES):
			if not (EDGES[src][dst] is None):
				NODES[src].edges.append(EDGES[src][dst])

	SORTED_FLOWS = []
	for dst in range(NUM_NODES):
		if not (FLOWS[SRC][dst] is None):
			SORTED_FLOWS.append(FLOWS[SRC][dst])
	SORTED_FLOWS.sort(key = lambda flow: flow.req)

	PATHS = [[[] for i in range(NUM_NODES)] for j in range(NUM_NODES)]

	Q = Queue()

	curFlow = 0
	
	for c in range(MAX_CAP + 1):

		for node in NODES:
			node.inQ = False
			node.parent = None

		NODES[SRC].inQ = True
		node = NODES[SRC]
		while not (node is None):
			for edge in node.edges:
				if (edge.cap >= c):
					dst = edge.dst
					if not NODES[dst].inQ:
						NODES[dst].parent = node
						Q.push(NODES[dst])
						NODES[dst].inQ = True
			node = Q.pop()

		while curFlow < NUM_NODES - 1:
			flow = SORTED_FLOWS[curFlow]
			if flow.req <= c:
				dst = flow.dst
				node = NODES[dst]
				while not (node.parent is None):
					PATHS[SRC][dst].append(node.id)
					node = node.parent
				PATHS[SRC][dst].append(node.id)
				curFlow = curFlow + 1
			else:
				break

	return PATHS



def new(EDGES, FLOWS, NUM_NODES, NUM_EDGES, MAX_CAP, SRC):
	pass


def main():

	if (len(sys.argv) < 5):
		print("Usage: %s <NUM_NODES> <NUM_EDGES> <MAX_CAP> <NUM_RUNS>" % sys.argv[0])
		return

	NUM_NODES = int(sys.argv[1])
	NUM_EDGES = int(sys.argv[2])
	MAX_CAP = int(sys.argv[3])
	NUM_RUNS = int(sys.argv[4])
	seed = int(time.time())
	#seed = 1362992649
	random.seed(seed)
	print("%d %d %d %d %d" % (NUM_NODES, NUM_EDGES, MAX_CAP, NUM_RUNS, seed))

	EDGES = [[None for i in range(NUM_NODES)] for j in range(NUM_NODES)]
	FLOWS = [[None for i in range(NUM_NODES)] for j in range(NUM_NODES)]

	# Firstly form a ring of max cap edges to ensure full connectivity
	numEdges = NUM_NODES
	for i in range(NUM_NODES - 1):
		EDGES[i][i + 1] = Edge(i, i + 1, MAX_CAP)
	EDGES[NUM_NODES - 1][0] = Edge(NUM_NODES - 1, 0, MAX_CAP)
	
	# Generate random edges
	while numEdges < NUM_EDGES:
		src = random.randint(0, NUM_NODES - 1)
		dst = random.randint(0, NUM_NODES - 1)
		if EDGES[src][dst] is None:
			cap = random.randint(1, MAX_CAP)
			if (src != dst):
				EDGES[src][dst] = Edge(src, dst, cap)
				numEdges = numEdges + 1

	for src in range(NUM_NODES):
		for dst in range(NUM_NODES):
			edge = EDGES[src][dst]
			if edge is None:
				debug("0 "),
			else:
				debug("%d " % edge.cap),
		debug('')


	# Generate flows
	for src in range(NUM_NODES):
		for dst in range(NUM_NODES):
			if (src != dst):
				FLOWS[src][dst] = Flow(src, dst, random.randint(1, MAX_CAP))

	for run in range(NUM_RUNS):
		for SRC in range(1):
			start = time.clock()
			paths = simple(EDGES, FLOWS, NUM_NODES, NUM_EDGES, MAX_CAP, SRC)
			elapsed = (time.clock() - start)
			print(elapsed)
			
			for dst in range(1, NUM_NODES):
				print("%d (%d): %s" % (dst, FLOWS[SRC][dst].req, paths[SRC][dst]))


if __name__ == "__main__":
	main()

