#!/usr/bin/python
import sys

INF = 9999
C = 9

class Cell:
	def __init__(self):
		self.recvBufT = INF
		self.recvBufB = INF
		self.recvBufL = INF
		self.recvBufR = INF
		self.recvT = INF
		self.recvB = INF
		self.recvL = INF
		self.recvR = INF
		self.val = INF

def main():
	
	if (len(sys.argv) != 2):
		print("Usage: %s <n>" % (sys.argv[0]))
		return
	N = int(sys.argv[1])

	M = [[Cell() for j in range(N)] for i in range(N)]
	D = [[INF for j in range(N)] for i in range(N)]

	for i in range(N):
		D[i][i] = 0;
	for i in range(N - 1):
		D[i][i + 1] = 1
	D[N - 1][0] = 1
	
	
	print("The distance matrix:")
	for i in range(N):
		for j in range(N):
			print(D[i][j]),
		print
	print


	for step in range(9 * N):
		
		print("======== step: %d ========" % (step))
		
		# Start by receiving all data from neighbours
		for i in range(N):
			iNext = i + 1
			if iNext == N: iNext = 0
			iPrev = i - 1
			if iPrev < 0: iPrev = N - 1

			for j in range(N):
				jNext = j + 1
				if jNext == N: jNext = 0
				jPrev = j - 1
				if jPrev < 0: jPrev = N - 1
		
				M[i][j].recvBufT = M[iPrev][j].recvT
				M[i][j].recvBufB = M[iNext][j].recvB
				M[i][j].recvBufL = M[i][jPrev].recvL
				M[i][j].recvBufR = M[i][jNext].recvR
		
		# Copy received data from buffer to actual registers
		for i in range(N):
			for j in range(N):
				M[i][j].recvT = M[i][j].recvBufT
				M[i][j].recvB = M[i][j].recvBufB
				M[i][j].recvL = M[i][j].recvBufL
				M[i][j].recvR = M[i][j].recvBufR
		
		# Load D from Top (skewed, only once)
		for j in range(N):
			row = step - j
			if (row >= 0) and (row < N): M[0][j].recvT = D[row][j]
		
		# Load D from Bottom (skewed, only once)
		for j in range(N):
			row = j - step + (2 * (N - 1 - j))
			if (row >= 0) and (row < N): M[N - 1][j].recvB = D[row][j]
		
		# Load D from Left (skewed, only once)
		for i in range(N):
			col = step - i
			if (col >= 0) and (col < N): M[i][0].recvL = D[i][col]
		
		# Load D from Right (skewed, only once)
		for i in range(N):
			col = i - step + (2 * (N - 1 - i))
			if (col >= 0) and (col < N): M[i][N - 1].recvR = D[i][col]
		
		# Perform the triple operation in all cells
		for i in range(N):
			for j in range(N):
				M[i][j].val = min(M[i][j].val, M[i][j].recvT + M[i][j].recvL)
				M[i][j].val = min(M[i][j].val, M[i][j].recvB + M[i][j].recvR)
		
		# Transmit the value of the cell at the right time.
		for i in range(N):
			for j in range(N):
				if (j + (2 * i) == step % N): M[i][j].recvT = M[i][j].val
				if (i + (2 * j) == step % N): M[i][j].recvL = M[i][j].val
				if ((N - 1 - j) + (2 * (N - 1 - i)) == step % N): M[i][j].recvB = M[i][j].val
				if ((N - 1 - i) + (2 * (N - 1 - j)) == step % N): M[i][j].recvR = M[i][j].val

		for i in range(N):
			for j in range(N):
				print(M[i][j].val),
			print

		raw_input()



if __name__ == "__main__":
	main()


