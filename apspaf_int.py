#!/usr/bin/python
import sys
import time
import random
import math

INF = 99999999

NUM_NODES = 0
NUM_EDGES = 0
MAX_CAP   = 0
MAX_INT   = 0

DEBUG = False
DEBUG_FLOW = 41
DEBUG_SRC = 3
DEBUG_DST = 1


def APSP(D, N):
	"""
	Solve APSP 
	"""
	T = [[D[i][j] for j in range(N)] for i in range(N)]
	for k in range(N):
		for i in range(N):
			for j in range(N):
				if T[i][k] + T[k][j] < T[i][j]:
					T[i][j] = T[i][k] + T[k][j]
	return T


def APSPAF_SF(C, D):
	"""
	Solve APSPAF by solving APSP for each maximal flow
	"""
	U = [[[-1 for j in range(NUM_NODES)] for i in range(NUM_NODES)] for f in range(MAX_CAP + 1)]
	for f in range(1, MAX_CAP + 1):
		Df = [[INF for j in range(NUM_NODES)] for i in range(NUM_NODES)]
		for i in range(NUM_NODES):
			for j in range(NUM_NODES):
				if C[i][j] >= f:
					Df[i][j] = D[i][j]
		U[f] = APSP(Df, NUM_NODES)
	return U


def APSPAF(C, D):
	"""
	Solve APSPAF with the new algorithm
	"""
	if DEBUG:
		print("================ EXPAND GRAPH ================")
	start = time.clock()
	numExp = NUM_NODES * (MAX_INT)
	CC = [[0 for j in range(numExp)] for i in range(numExp)]
	DD = [[INF for j in range(numExp)] for i in range(numExp)]
	for i in range(numExp):
		CC[i][i] = INF
		DD[i][i] = 0
	i = 0
	# create artificial vertices and link them up with (1, INF) edges
	while i < numExp:
		ii = i + MAX_INT
		for j in range(i, ii - 1):
			#print("%d, %d" % (i, j))
			CC[j][j + 1] = INF
			DD[j][j + 1] = 1
		i = ii
	# go through all edges and create artificial edges accordingly
	for i in range(NUM_NODES):
		for j in range(NUM_NODES):
			if i != j:
				if D[i][j] < INF:
					cap = C[i][j]
					src = (i * MAX_INT) + D[i][j] - 1
					dst = (j * MAX_INT)
					CC[src][dst] = cap
					DD[src][dst] = 1

	'''
	print("CC")
	for i in range(numExp):
		for j in range(numExp):
			print("%d " % CC[i][j]),
		print
	print("DD")
	for i in range(numExp):
		for j in range(numExp):
			print("%d " % DD[i][j]),
		print
	'''

	elapsed = (time.clock() - start)
	#print("Time taken to expand graph: %.2f" % (elapsed))
		
	start = time.clock()
	# Initialization for the acceleration phase
	Cl = [[CC[i][j] for j in range(numExp)] for i in range(numExp)]
	T = [[[] for j in range(numExp)] for i in range(numExp)]
	for i in range(numExp):
		for j in range(numExp):
			if i != j:
				if CC[i][j] > 0:
					if (j % MAX_INT) == 0: # if j is a real vertex
						T[i][j].insert(0, (1, 1, CC[i][j]))
					else:
						T[i][j].insert(0, (0, 1, CC[i][j]))
	elapsed = (time.clock() - start)
	#print("Time taken to initialize for acceleration: %.2f" % (elapsed))

	start = time.clock()
	r = int(math.sqrt(MAX_CAP) / MAX_INT)
	if r < MAX_INT:
		r = MAX_INT
	if DEBUG:
		print("================ ACCELERATION PHASE (r = %d) ================" % (r))
	for l in range(2, r + 1):
		if DEBUG:
			print("l = %d" % (l))
		'''
		print
		for i in range(numExp):
			for j in range(numExp):
				print("%d " % (Cl[i][j])),
			print
		'''
		updateList = []
		for i in range(numExp):
			for j in range(numExp):
				c = Cl[i][j]
				updated = False
				for k in range(numExp):
					if c < min(Cl[i][k], CC[k][j]):
						updated = True
						c = min(Cl[i][k], CC[k][j])
						w = k
				if updated:
					(h, d, f) = T[i][w][0]
					updateList.append((i, j, c, w, h))
		for (i, j, c, w, h) in updateList:
			Cl[i][j] = c
			#if DEBUG and (i == DEBUG_SRC * MAX_INT) and (j == DEBUG_DST * MAX_INT):
			if DEBUG and (i == 12) and (j == 18):
				print("i = %d, j = %d, c = %d, w = %d, h = %d" % (i, j, c, w, h))
				print("T[%d][%d] = %s" % (i, j, T[i][j]))
				print("T[%d][%d] = %s" % (i, w, T[i][w]))
			if (j % MAX_INT) == 0: # i.e. j is a real vertex
				T[i][j].insert(0, (h + 1, l, Cl[i][j]))
			else:
				T[i][j].insert(0, (h, l, Cl[i][j]))
	elapsed = (time.clock() - start)
	#print("Time taken for acceleration phase: %.2f" % (elapsed))

	'''
	print("Cl (real vertices only)")
	for i in range(NUM_NODES):
		for j in range(NUM_NODES):
			print("%d " % Cl[i * MAX_INT][j * MAX_INT]),
		print
	'''
	
	# Initialize for cruising phase
	start = time.clock()
	P = [[[INF for j in range(NUM_NODES)] for i in range(NUM_NODES)] for f in range(MAX_CAP + 1)]
	Q = [[[INF for j in range(NUM_NODES)] for i in range(NUM_NODES)] for f in range(MAX_CAP + 1)]
	for f in range(1, MAX_CAP + 1):
		for i in range(NUM_NODES):
			P[f][i][i] = 0
			Q[f][i][i] = 0

	for i in range(NUM_NODES):
		ii = i * MAX_INT
		for j in range(NUM_NODES):
			jj = j * MAX_INT
			if len(T[ii][jj]) > 0:
				for (h, d, f) in T[ii][jj]:
					#print("h = %d, d = %d, f = %d" % (h, d, f))
					if DEBUG and (i == DEBUG_SRC) and (j == DEBUG_DST):
						print("h = %d, d = %d, f = %d" % (h, d, f))
					for flow in range(1, f + 1):
						P[flow][i][j] = d
						Q[flow][i][j] = h
	elapsed = (time.clock() - start)
	#print("Time taken to initialize for cruising phase: %.2f" % (elapsed))

	if DEBUG:
		print("==== P[%d] ====" % (DEBUG_FLOW))
		for i in range(NUM_NODES):
			for j in range(NUM_NODES):
				print("%d " % P[DEBUG_FLOW][i][j]),
			print
		print("==== Q[%d] ====" % (DEBUG_FLOW))
		for i in range(NUM_NODES):
			for j in range(NUM_NODES):
				print("%d " % Q[DEBUG_FLOW][i][j]),
			print

	if DEBUG:
		print("========================== CRUISING PHASE ===================")

	start = time.clock()
	# Cruising phase
	l = r / MAX_INT
	#print("l = %d" % (l))
	while l < NUM_NODES:
		lmax = int(math.ceil(l * 1.5))
		if lmax > NUM_NODES:
			lmax = NUM_NODES
		lmin = int(math.ceil(l / 2.0))

		if DEBUG:
			print("l = %d, lmax = %d, lmin = %d" % (l, lmax, lmin))
		
		for f in range(1, MAX_CAP + 1):

			S = [[] for i in range(NUM_NODES)]
			for i in range(NUM_NODES):
				COUNT = [0 for plen in range(NUM_NODES)]
				for j in range(NUM_NODES):
					plen = Q[f][i][j]
					if plen >= lmin and plen <= l:
						COUNT[plen] = COUNT[plen] + 1
				occ = NUM_NODES
				minOccLen = 0
				for plen in range(lmin, l + 1):
					if COUNT[plen] > 0 and COUNT[plen] < occ:
						occ = COUNT[plen]
						minOccLen = plen
				for j in range(NUM_NODES):
					if Q[f][i][j] == minOccLen:
						S[i].append(j)

			updateList = []
			for i in range(NUM_NODES):
				for j in range(NUM_NODES):
					if DEBUG and (f == DEBUG_FLOW) and (i == DEBUG_SRC) and (j == DEBUG_DST):
						print(S[DEBUG_SRC])
					if i != j:
						update = False
						minD = P[f][i][j]
						minL = NUM_NODES
						for k in S[i]:
							#if DEBUG and (f == DEBUG_FLOW) and (i == DEBUG_SRC) and (j == DEBUG_DST):
							#	print("%d = %d + %d" % (P[f][i][k] + P[f][k][j], P[f][i][k], P[f][k][j]))
							dist = P[f][i][k] + P[f][k][j]
							plen = Q[f][i][k] + Q[f][k][j]
							if dist < minD:
								update = True
								minD = dist
								minL = plen
							elif dist == minD:
								if plen < minL:
									update = True
									minL = plen
						if update:
							updateList.append((i, j, minD, minL))

			for (i, j, dist, plen) in updateList:
				#print("i = %d, j = %d, dist = %d, plen = %d" % (i, j, dist, plen))
				P[f][i][j] = dist
				Q[f][i][j] = plen

			if DEBUG and f == DEBUG_FLOW:
				print("P")
				for i in range(NUM_NODES):
					for j in range(NUM_NODES):
						print("%d " % P[DEBUG_FLOW][i][j]),
					print

				print("Q")
				for i in range(NUM_NODES):
					for j in range(NUM_NODES):
						print("%d " % Q[DEBUG_FLOW][i][j]),
					print

		l = lmax
	elapsed = (time.clock() - start)
	#print("Time taken for cruising phase: %.2f" % (elapsed))
		
	return P


def main():

	if (len(sys.argv) != 6):
		print("Usage: %s <NUM_NODES> <NUM_EDGES> <MAX_CAP> <MAX_INT> <NUM_RUNS>" % sys.argv[0])
		return

	global NUM_NODES
	global NUM_EDGES
	global MAX_CAP
	global MAX_INT

	NUM_NODES = int(sys.argv[1])
	NUM_EDGES = int(sys.argv[2])
	MAX_CAP   = int(sys.argv[3])
	MAX_INT   = int(sys.argv[4])
	NUM_RUNS  = int(sys.argv[5])

	print("NUM_NODES = %d, NUM_EDGES = %d, MAX_CAP = %d, MAX_INT = %d, MAX_RUNS = %d" % (NUM_NODES, NUM_EDGES, MAX_CAP, MAX_INT, NUM_RUNS))

	if DEBUG:
		print("DEBUGGING ENABLED: flow = %d, src = %d, dst = %d" % (DEBUG_FLOW, DEBUG_SRC, DEBUG_DST))
	
	for run in range(NUM_RUNS):
		seed = int(time.time())
		#seed = 1372824471
		random.seed(seed)
		print("Run #%d: seed = %d" % (run, seed))

		C = [[0 for i in range(NUM_NODES)] for j in range(NUM_NODES)]
		D = [[INF for i in range(NUM_NODES)] for j in range(NUM_NODES)]

		# Start with a MAX_CAP/MAX_COST ring then generate some more random edges
		numEdges = NUM_NODES
		for i in range(NUM_NODES):
			C[i][i] = INF
			D[i][i] = 0
		for i in range(NUM_NODES - 1):
			C[i][i + 1] = MAX_CAP
			D[i][i + 1] = MAX_INT
		C[NUM_NODES - 1][0] = MAX_CAP
		D[NUM_NODES - 1][0] = MAX_INT
		while numEdges < NUM_EDGES:
			src = random.randint(0, NUM_NODES - 1)
			dst = random.randint(0, NUM_NODES - 1)
			if (src != dst) and (C[src][dst] == 0):
				numEdges = numEdges + 1
				C[src][dst] = random.randint(1, MAX_CAP)
				D[src][dst] = random.randint(1, MAX_INT)

		if DEBUG:
			print("C (DEBUG_FLOW = %d)" % (DEBUG_FLOW))
			for i in range(NUM_NODES):
				for j in range(NUM_NODES):
					if C[i][j] >= DEBUG_FLOW:
						print("%d " % (C[i][j])),
					else:
						print(0),
				print
			print("D (DEBUG_FLOW = %d)" % (DEBUG_FLOW))
			for i in range(NUM_NODES):
				for j in range(NUM_NODES):
					if C[i][j] >= DEBUG_FLOW:
						print("%d " % (D[i][j])),
					else:
						print(INF),
				print

		start = time.clock()
		U1 = APSPAF_SF(C, D)
		elapsed = (time.clock() - start)
		if not DEBUG:
			print("Floyd time taken: %.3f" % (elapsed))

		'''
		if DEBUG:
			for i in range(NUM_NODES):
				for j in range(NUM_NODES):
					print("%d " % U1[DEBUG_FLOW][i][j]),
				print
		'''
				
		start = time.clock()
		U2 = APSPAF(C, D)
		elapsed = (time.clock() - start)
		if not DEBUG:
			print("New time taken: %.3f" % (elapsed))

		'''
		if DEBUG:
			for i in range(NUM_NODES):
				for j in range(NUM_NODES):
					print("%d " % U2[DEBUG_FLOW][i][j]),
				print
		'''
				
		error = False
		for f in range(1, MAX_CAP + 1):
			for i in range(NUM_NODES):
				for j in range(NUM_NODES):
					if (U1[f][i][j] != U2[f][i][j]):
						print("!!! ERROR: f = %d, i = %d, j = %d, U1 = %d, U2 = %d" % (f, i, j, U1[f][i][j], U2[f][i][j]))
						error = True
		
		if error:
			break

		time.sleep(1)

if __name__ == "__main__":
	main()

