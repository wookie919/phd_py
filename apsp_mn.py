#!/usr/bin/python
import sys
import time
import random

INF = 9999999999999999
C = 99999999

def floyd(D, N):
	for k in range(N):
		for i in range(N):
			for j in range(N):
				D[i][j] = min(D[i][j], D[i][k] + D[k][j])

class Edge:
	def __init__(self, srcV, dstV, cost):
		self.srcV = srcV
		self.dstV = dstV
		self.cost = cost

class Vertex:
	def __init__(self):
		self.edgesOut = []
		self.edgesIn = []


def new(D, N, A):

	edges = []

	for i in range(N):
		for j in range(N):
			if D[i][j] > 0 and D[i][j] < INF:
				edges.append(Edge(i, j, D[i][j]))
	
	edges.sort(key = lambda edge: edge.cost)

	diff = True
	loop = 0
	while diff:
		loop = loop + 1
		for edge in edges:
			srcV = edge.srcV
			dstV = edge.dstV
			for i in range(N):
				if D[i][srcV] < INF:
					D[i][dstV] = min(D[i][dstV], D[i][srcV] + D[srcV][dstV])
			for j in range(N):
				if D[dstV][j] < INF:
					D[srcV][j] = min(D[srcV][j], D[srcV][dstV] + D[dstV][j])

		diff = False
		for i in range(N):
			for j in range(N):
				if D[i][j] != A[i][j]:
					diff = True
					break
			if diff == True:
				break

	print("Number of iterations: %d" % (loop))


def main():
	
	if (len(sys.argv) < 4):
		print("Usage: %s <N> <M> <R>" % sys.argv[0])
		return

	N = int(sys.argv[1])
	M = int(sys.argv[2])
	R = int(sys.argv[3])
	seed = int(time.time())
	
	#seed = 1369712647
	random.seed(seed)
	
	print("N = %d, M = %d, R = %d, seed = %d" % (N, M, R, seed))

	for r in range(R):
	
		D1 = [[INF for i in range(N)] for j in range(N)]
		D2 = [[INF for i in range(N)] for j in range(N)]
		
		for i in range(N - 1):
			D1[i][i] = 0
			D2[i][i] = 0
			D1[i][i + 1] = random.randint(1, C)
			D2[i][i + 1] = D1[i][i + 1]
		D1[N - 1][N - 1] = 0
		D2[N - 1][N - 1] = 0
		D1[N - 1][0] = random.randint(1, C)
		D2[N - 1][0] = D1[N - 1][0]

		m = N
		while m < M:
			i = random.randint(0, N - 1)
			j = random.randint(0, N - 1)
			if (i != j and D1[i][j] == INF):
				D1[i][j] = random.randint(1, C)
				D2[i][j] = D1[i][j]
				m = m + 1

		'''
		print("The distance matrix:")
		for i in range(N):
			for j in range(N):
				print(D1[i][j]),
			print
		'''
				
		# Solve using Floyd
		start = time.clock()
		floyd(D1, N)
		elapsed = time.clock() - start
		print("Time taken to solve using Floyd: %.4f" % (elapsed))
		
		# Solve using new method
		start = time.clock()
		new(D2, N, D1)
		elapsed = time.clock() - start
		print("Time taken to solve using the new method: %.4f" % (elapsed))

if __name__ == "__main__":
	main()
