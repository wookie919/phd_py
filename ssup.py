#!/usr/bin/python
import sys
import os
import time
import random

class Queue:
		def __init__(self):
			self.in_stack = []
			self.out_stack = []
		def push(self, obj):
			self.in_stack.append(obj)
		def pop(self):
			if not self.out_stack:
				while self.in_stack:
					self.out_stack.append(self.in_stack.pop())
			if not self.out_stack:
				return None
			else:
				return self.out_stack.pop()


def simple(NUM_NODES, NUM_EDGES, EDGE_CAPS, SRC, PATHS):

	class Edge:
		def __init__(self, src, dst, cap):
			self.src = src
			self.dst = dst
			self.cap = cap

	class Node:
		def __init__(self, id):
			self.id = id
			self.cap = 0
			self.parent = None
			self.edges = []
			self.inQ = False

	EDGES = [[None for dst in range (NUM_NODES)] for src in range(NUM_NODES)]
	for src in range(NUM_NODES):
		for dst in range(NUM_NODES):
			if EDGE_CAPS[src][dst] > 0:
				EDGES[src][dst] = Edge(src, dst, EDGE_CAPS[src][dst])
	
	NODES = [Node(n) for n in range(NUM_NODES)]
	for src in range(NUM_NODES):
		for dst in range(NUM_NODES):
			if not (EDGES[src][dst] is None):
				NODES[src].edges.append(EDGES[src][dst])

	EDGES_SORTED = []
	for src in range(NUM_NODES):
		for dst in range(NUM_NODES):
			if EDGES[src][dst] is not None:
				EDGES_SORTED.append(EDGES[src][dst])
	EDGES_SORTED.sort(key = lambda edge: edge.cap)

	Q = Queue()
	for m in range(NUM_EDGES):

		cap = EDGES_SORTED[m].cap
		for node in NODES:
			node.inQ = False
			node.parent = None

		NODES[SRC].inQ = True
		node = NODES[SRC]
		while not (node is None):
			for edge in node.edges:
				if (edge.cap >= cap):
					dst = edge.dst
					if not NODES[dst].inQ:
						NODES[dst].parent = node
						Q.push(NODES[dst])
						NODES[dst].inQ = True
			node = Q.pop()

		for node in NODES:
			dst = node.id
			if dst != SRC:
				dist = 0
				curNode = node.parent
				while curNode is not None:
					dist = dist + 1
					curNode = curNode.parent
				if PATHS[SRC][dst][dist] < cap:
					PATHS[SRC][dst][dist] = cap




def new(NUM_NODES, NUM_EDGES, EDGE_CAPS, SRC, PATHS):

	class Edge:
		def __init__(self, src, dst, cap):
			self.src = src
			self.dst = dst
			self.cap = cap

	class Node:
		def __init__(self, id):
			self.id = id
			self.cap = 0
			self.dist = 0
			self.parent = None
			self.edgesIn = []
			self.inTree = False

	EDGES = [[None for dst in range (NUM_NODES)] for src in range(NUM_NODES)]
	for src in range(NUM_NODES):
		for dst in range(NUM_NODES):
			if EDGE_CAPS[src][dst] > 0:
				EDGES[src][dst] = Edge(src, dst, EDGE_CAPS[src][dst])

	EDGES_SORTED = []
	for src in range(NUM_NODES):
		for dst in range(NUM_NODES):
			if EDGES[src][dst] is not None:
				EDGES_SORTED.append(EDGES[src][dst])
	EDGES_SORTED.sort(key = lambda edge: edge.cap)

	NODES = [Node(n) for n in range(NUM_NODES)]
	for edge in EDGES_SORTED:
		NODES[edge.dst].edgesIn.append(edge)

	Q = [Queue() for d in range(NUM_NODES)]

	srcNode = NODES[SRC]
	srcNode.cap = 1
	srcNode.inTree = True

	for EDGE in EDGES_SORTED:

		CAP = EDGE.cap

		# Remove low capacity nodes from the Tree and add them to Q
		for node in NODES:
			if node.cap < CAP:
				node.parent = None
				node.dist = node.dist + 1
				Q[node.dist].push(node)

		# Add all nodes in Q back to the Tree at the shortest distance possible
		for d in range(1, NUM_NODES):
			node = Q[d].pop()
			while node is not None:
				for edge in node.edgesIn:
					if edge.cap >= CAP:
						parent = NODES[edge.src]
						if (parent.dist == node.dist - 1):
							if edge.cap < parent.cap:
								minCap = edge.cap
							else:
								minCap = parent.cap
							if minCap > node.cap:
								node.parent = parent
								node.cap = minCap
				if node.parent is not None:
					PATHS[SRC][node.id][node.dist] = node.cap
				else:
					node.dist = node.dist + 1
					Q[node.dist].push(node)
				node = Q[d].pop()



def main():

	if (len(sys.argv) < 4):
		print("Usage: %s <NUM_NODES> <NUM_EDGES> <NUM_RUNS>" % sys.argv[0])
		return

	NUM_NODES = int(sys.argv[1])
	NUM_EDGES = int(sys.argv[2])
	NUM_RUNS = int(sys.argv[3])
	seed = int(time.time())
	#seed = 1362992649
	random.seed(seed)
	print("%d %d %d %d" % (NUM_NODES, NUM_EDGES, NUM_RUNS, seed))

	EDGE_CAPS = [[0 for i in range(NUM_NODES)] for j in range(NUM_NODES)]

	# Start with a ring and generate some more random edges
	numEdges = NUM_NODES
	for i in range(NUM_NODES - 1):
		EDGE_CAPS[i][i + 1] = 1.00
	EDGE_CAPS[NUM_NODES - 1][0] = 1.00
	while numEdges < NUM_EDGES:
		src = random.randint(0, NUM_NODES - 1)
		dst = random.randint(0, NUM_NODES - 1)
		if (src != dst) and (EDGE_CAPS[src][dst] == 0):
			EDGE_CAPS[src][dst] = random.random()
			numEdges = numEdges + 1

	'''
	for src in range(NUM_NODES):
		for dst in range(NUM_NODES):
			print("%.2f " % EDGE_CAPS[src][dst]),
		print
	'''

	PATH1 = [[[0 for dist in range(NUM_NODES)] for dst in range(NUM_NODES)] for src in range(NUM_NODES)]
	PATH2 = [[[0 for dist in range(NUM_NODES)] for dst in range(NUM_NODES)] for src in range(NUM_NODES)]
	
	for run in range(NUM_RUNS):

		start = time.clock()
		for SRC in range(NUM_NODES):
			simple(NUM_NODES, NUM_EDGES, EDGE_CAPS, SRC, PATH1)
		elapsed = (time.clock() - start)
		print(elapsed)

		start = time.clock()
		for SRC in range(NUM_NODES):
			new(NUM_NODES, NUM_EDGES, EDGE_CAPS, SRC, PATH2)
		elapsed = (time.clock() - start)
		print(elapsed)

		for src in range(NUM_NODES):
			for dst in range(NUM_NODES):
				for d in range(NUM_NODES):
					if PATH1[SRC][dst][d] != PATH2[SRC][dst][d]:
						print("!!! ERROR !!!: %d %d %d" % (SRC, dst, d))
						print("%.2f %.2f" % (PATH1[SRC][dst][d], PATH2[SRC][dst][d]))

if __name__ == "__main__":
	main()

