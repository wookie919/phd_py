#!/usr/bin/python
import sys
import time
import random

INF = 9999

N = 0
M = 0
C = 0
R = 0


class Node():
	def __init__(self, id):
		self.id = id
		self.parent = None
		self.cost = 0
		self.flow = 0


def full_tree(EDGES):
	
	IN  = [[] for i in range(N)]
	OUT = [[] for i in range(N)]
	for i in range(N):
		for j in range(N):
			if (EDGES[i][j] is not None) and (j != 0):
				IN[j].append(i)
				OUT[i].append(j)


	for i in range(N):
		print(OUT[i])

	NC = N * C
	DIST_BUCKET = [[] for d in range(NC + 1)]

	# Start by initializing the source node
	s = Node(0)
	s.flow = M + 1
	DIST_BUCKET[0].append(s) 

	MAX_FLOW = [0 for i in range(N)]

	maxCost = 0
	for d in range(NC):
		for srcNode in DIST_BUCKET[d]:
			for dstNum in OUT[srcNode.id]:
				(cost,flow) = EDGES[srcNode.id][dstNum]
				newFlow = min(srcNode.flow, flow)
				if newFlow > MAX_FLOW[dstNum]:
					MAX_FLOW[dstNum] = newFlow
					newCost = srcNode.cost + cost
					dstNode = Node(dstNum)
					dstNode.parent = srcNode
					dstNode.cost = newCost
					dstNode.flow = newFlow
					DIST_BUCKET[dstNode.cost].append(dstNode)
					if newCost > maxCost:
						maxCost = newCost

	TUPLES = [[] for i in range(N)]

	for d in range(1,maxCost + 1):
		for node in DIST_BUCKET[d]:
			if len(TUPLES[node.id]) == 0:
				TUPLES[node.id].append((node.cost, node.flow))
			else:
				(cost, flow) = TUPLES[node.id][-1]
				if flow < node.flow:
					if cost == node.cost:
						TUPLES[node.id].pop()
					TUPLES[node.id].append((node.cost, node.flow))

	for i in range(N):
		print TUPLES[i]

	return TUPLES



class Triplet():
	def __init__(self, v, d, f):
		self.v = v
		self.d = d
		self.f = f
		self.inQ = False





def cost_key(EDGES, SRC):

	OUT = [[] for i in range(N)]
	for v in range(N):
		for w in range(N):
			if (EDGES[v][w] is not None) and (w != SRC):
				OUT[v].append(w)

	CN = C * N

	S = [[] for v in range(N)]
	Q = [[] for f in range(CN + 1)]
	P = [[None for v in range(N)] for d in range(CN + 1)]

	P[0][SRC] = Triplet(SRC, 0, M)
	P[0][SRC].inQ = True
	Q[0].append(P[0][SRC])

	dist = 0
	while dist <= (CN - C):
		#print('======== Distance from SRC: %d ========' % (dist))
		while len(Q[dist]) > 0:
			triplet = Q[dist].pop()
			triplet.inQ = False
			v = triplet.v
			d = triplet.d
			f = triplet.f
			#print('Triplet (%d,%d,%d) popped from Q' % (v, d, f))
			for w in OUT[v]:
				(eCost, eFlow) = EDGES[v][w]
				ff = min(f, eFlow)
				dd = d + eCost
				if P[dd][w] is None:
					#print('Triplet (%d, %d, %d) created' % (w, dd, ff))
					P[dd][w] = Triplet(w, dd, ff)
					P[dd][w].inQ = True
					Q[dd].append(P[dd][w])
				elif P[dd][w].f < ff:
					#print('Triplet (%d, %d, %d) updated to (%d, %d, %d)' % (w, dd, f, w, dd, ff))
					P[dd][w].f = ff
					if not P[dd][w].inQ:
						P[dd][w].inQ = True
						Q[dd].append(P[dd][w])

			if len(S[v]) == 0:
				S[v].append((d,f))
			else:
				(dd, ff) = S[v][-1]
				if f > ff:
					if d == dd:
						S[v].pop()
					S[v].append((d,f))
		dist = dist + 1

	S[0] = []
	for v in range(N):
		print(S[v])

	return S




def cap_key(EDGES, SRC):

	class Triplet():
		def __init__(self, v, d, f):
			self.v = v
			self.d = d
			self.f = f
			self.inQ = False

	OUT = [[] for i in range(N)]
	for v in range(N):
		for w in range(N):
			if (EDGES[v][w] is not None) and (w != SRC):
				OUT[v].append(w)

	S = [[] for v in range(N)]
	Q = [[] for f in range(M + 1)]
	P = [[None for v in range(N)] for f in range(M + 1)]

	P[M][SRC] = Triplet(SRC, 0, M)
	P[M][SRC].inQ = True
	Q[M].append(P[M][SRC])

	flow = M
	while flow > 0:
		#print('======== Maximal Flow: %d ========' % (flow))
		while len(Q[flow]) > 0:
			triplet = Q[flow].pop()
			triplet.inQ = False
			v = triplet.v
			d = triplet.d
			f = triplet.f
			#print('Triplet (%d,%d,%d) popped from Q' % (v, d, f))
			for w in OUT[v]:
				(eCost, eFlow) = EDGES[v][w]
				ff = min(f, eFlow)
				dd = d + eCost
				if P[ff][w] is None:
					#print('Triplet (%d, %d, %d) created' % (w, dd, ff))
					P[ff][w] = Triplet(w, dd, ff)
					P[ff][w].inQ = True
					Q[ff].append(P[ff][w])
				elif P[ff][w].d > dd:
					P[ff][w].d = dd
					if not P[ff][w].inQ:
						P[ff][w].inQ = True
						Q[ff].append(P[ff][w])
			if len(S[v]) == 0:
				S[v].append((d,f))
			else:
				(dd, ff) = S[v][-1]
				if d < dd:
					if f == ff:
						S[v].pop()
					S[v].append((d,f))
		flow = flow - 1

	S[0] = []
	for v in range(N):
		S[v].reverse()
		print(S[v])

	return S



def real_cap(EDGES, SRC):

	OUT = [[] for i in range(N)]
	for v in range(N):
		for w in range(N):
			if (EDGES[v][w] is not None) and (w != SRC):
				OUT[v].append(w)


	InQ = [False for v in range(N)]
	S = [[] for v in range(N)]
	D = [[INF for v in range(N)] for f in range(M + 1)]
	D[M][SRC] = 0

	for f in range(M, 0, -1):
		Q = []
		for v in range(N):
			if D[f][v] < INF:
				Q.append(v)
				InQ[v] = True
		while len(Q) > 0:
			v = Q.pop()
			print("f: %d, pop: %d" % (f, v))
			InQ[v] = False
			for w in OUT[v]:
				(eCost, eFlow) = EDGES[v][w]
				dd = D[f][v] + eCost
				ff = min(f, eFlow)
				if dd < D[ff][w]:
					D[ff][w] = dd
					if f == ff and not InQ[w]:
						Q.append(w)
						InQ[w] = True

	for f in range(M, 0, -1):
		for v in range(N):
			if D[f][v] < INF:
				d = D[f][v]
				print('v: %d d: %d, f: %d' % (v, d, f))
				if len(S[v]) == 0:
					S[v].append((d,f))
				else:
					(d0, f0) = S[v][0]
					if d < d0:
						S[v].insert(0, (d, f))

	S[0] = []
	for v in range(N):
		print(S[v])

	return S





def main():
	if (len(sys.argv) != 5):
		print("Usage: %s <NUM_NODES> <NUM_EDGES> <MAX_INT> <NUM_RUNS>" % sys.argv[0])
		return

	global N
	global M
	global C
	global R

	N = int(sys.argv[1])
	M = int(sys.argv[2])
	C = int(sys.argv[3])
	R = int(sys.argv[4])
	seed = int(time.time())
	#seed = 1377751890
	random.seed(seed)
	print("N = %d, M = %d, C = %d, R = %d, seed = %d" % (N, M, C, R, seed))

	EDGES = [[None for j in range(N)] for i in range(N)]
	# Start with a ring and generate some more random edges
	numEdges = N
	for i in range(N - 1):
		EDGES[i][i + 1] = (C,M)
	EDGES[N - 1][0] = (C,M)
	while numEdges < M:
		src = random.randint(0, N - 1)
		dst = random.randint(0, N - 1)
		if (src != dst) and (EDGES[src][dst] is None):
			EDGES[src][dst] = (random.randint(1, C), random.randint(1, M))
			numEdges = numEdges + 1

	for i in range(N):
		for j in range(N):
			if EDGES[i][j] is None:
				print("(-1,-1)"),
			else:
				print("(%d,%d)" % (EDGES[i][j][0], EDGES[i][j][1])),
		print

	for run in range(R):
		start = time.clock()
		S1 = cost_key(EDGES, 0)
		print(time.clock() - start)
		start = time.clock()
		S2 = cap_key(EDGES, 0)
		print(time.clock() - start)
		start = time.clock()
		S3 = real_cap(EDGES, 0)
		print(time.clock() - start)


		for v in range(N):
			if (len(S1[v]) != len(S2[v])) or (len(S1[v]) != len(S3[v])):
				print("!!! ERROR !!!: S lengths not equal")
				return
			for i in range(len(S1[v])):
				(d1, f1) = S1[v][i]
				(d2, f2) = S2[v][i]
				(d3, f3) = S3[v][i]
				if (d1 != d2) or (f1 != f2) or (d1 != d3) or (f1 != f3):
					print("!!! ERROR !!!")
					return

if __name__ == "__main__":
	main()

