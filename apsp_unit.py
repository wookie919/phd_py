#!/usr/bin/python
import sys
import time
import random
import math

INF = 99

def naive(D, N):

	T = [[0 for i in range(N)] for j in range(N)]

	l = 1
	while l < N:
		l = l * 2

		for i in range(N):
			for j in range(N):
				T[i][j] = D[i][j]

		for i in range(N):
			for j in range(N):
				if (i != j):
					oldDist = D[i][j]
					for k in range(N):
						if (k != i) and (k != j):
							newDist = D[i][k] + D[k][j]
							if oldDist > newDist:
								oldDist = newDist
					T[i][j] = oldDist

		for i in range(N):
			for j in range(N):
				D[i][j] = T[i][j]




def BMM(C, A, B, W, N, debug=False):
	'''
	C = A * B
	W = witness
	N = matrix size
	'''
	for i in range(N):
		for j in range(N):
			if (i != j):
				for k in range(N):
					if (i != k) and (k != j):
						if (A[i][k] == 1) and (B[k][j] == 1):
							C[i][j] = 1
							W[i][j] = k
							if debug:
									print("i = %d, j = %d, k = %d" % (i, j, k))
							break


def CONCAT(D, P, i, j, k):
	'''
	D = distance matrix
	P = Explicit path list
	i = source
	j = destination
	k = via
	'''
	
	path = P[i][k][:-1] + P[k][j]
	print(path)
	plen = len(path) - 1
	d = plen / 2
	while (d < plen):
		d = d + 1
		for idx in range(0, plen - d + 1):
			src = path[idx]
			dst = path[idx + d]
			if D[src][dst] > d:
				#print("src: %d, dst: %d, D[src][dst]: %d, d: %d" % (src, dst, D[src][dst], d))
				D[src][dst] = d
	#D[i][j] = plen
	P[i][j] = path


def fast(D, N):

	P = [[None for i in range(N)] for j in range(N)]

	A = [[0 for i in range(N)] for j in range(N)]
	for i in range(N):
		for j in range(N):
			if D[i][j] == 1:
				A[i][j] = 1
				P[i][j] = [i, j]

	l = 1
	while (l < 8):

		Dk = [[0 for i in range(N)] for j in range(N)]
		Dl = [[0 for i in range(N)] for j in range(N)]
		for i in range(N):
			for j in range(N):
				if D[i][j] <= l:
					Dl[i][j] = 1
				if D[i][j] < l:
					Dk[i][j] = 1

		Bk = [[0 for i in range(N)] for j in range(N)]
		W = [[INF for i in range(N)] for j in range(N)]
		BMM(Bk, Dk, Dl, W, N)
		for i in range(N):
			for j in range(N):
				if Dk[i][j] == 1 or Dl[i][j] == 1:
					Bk[i][j] = 1

		Bl = [[0 for i in range(N)] for j in range(N)]
		for i in range(N):
			for j in range(N):
				if D[i][j] == l:
					Bl[i][j] = 1
		
		'''
		for i in range(N):
			for j in range(N):
				print("%d " % (Bl[i][j])),
			print
		'''
		
		R = [[0 for i in range(N)] for j in range(N)]
		W = [[INF for i in range(N)] for j in range(N)]
		BMM(R, Bl, Bl, W, N)

		'''
		for i in range(N):
			for j in range(N):
				print("%d " % (R[i][j])),
			print
		'''

		for i in range(N):
			for j in range(N):
				if (i == 0) and (j == 8):
					print("R[i][j]: %d, Bk[i][j]: %d, Bl[i][j]: %d" % (R[i][j], Bk[i][j], Bl[i][j]))
				if (R[i][j] == 1) and (Bk[i][j] == 0) and (Bl[i][j] == 0):
					#print("i: %d, j: %d, k: %d" % (i, j, W[i][j]))
					CONCAT(D, P, i, j, W[i][j])

		l = l * 2
		
		'''
		for i in range(N):
			for j in range(N):
				print("%d " % (D[i][j])),
			print
		'''




	"""
	print("Cruising phase, l = t^2 = %d" % l)
	while l < N:

		R = [[0 for i in range(N)] for j in range(N)]
		for i in range(N):
			for j in range(N):
				if D[i][j] > 0:
					R[i][j] = 1
				else:
					R[i][j] = 0

		lmin = int(math.ceil(l / 2.0))
		lnew = int(math.ceil(l * 1.5))

		# Determine bridging set Si for each row
		S = [[] for i in range(N)]
		for i in range(N):
			O = [0 for d in range(N)]
			for j in range(N):
				d = D[i][j]
				if (d >= lmin) and (d <= l):
					O[d] = O[d] + 1
			occ = N
			dist = 0
			lmax = l + 1
			if lmax > N:
				lmax = N
			for d in range(lmin, lmax):
				if (O[d] > 0) and (O[d] < occ):
					occ = O[d]
					dist = d
			for j in range(N):
				if D[i][j] == dist:
					#print("S[%d]: %d" % (i,j))
					S[i].append(j)

		DT = [[0 for i in range(N)] for j in range(N)]
		for i in range(N):
			for j in range(N):
				if i != j:
					oldDist = D[i][j]
					for k in S[i]:
						if (k != i) and (k != j):
							if (D[i][k] > 0) and (D[k][j] > 0):
								newDist = D[i][k] + D[k][j]
								if (newDist <= lnew):
									if (oldDist == 0) or (oldDist > newDist):
										oldDist = newDist
					DT[i][j] = oldDist

		for i in range(N):
			for j in range(N):
				D[i][j] = DT[i][j]

		l = lnew

		'''
		for i in range(N):
			for j in range(N):
				print("%d " % (D[i][j])),
			print
		'''
	"""

def main():

	if (len(sys.argv) < 4):
		print("Usage: %s <N> <M> <R>" % sys.argv[0])
		return

	N = int(sys.argv[1])
	M = int(sys.argv[2])
	R = int(sys.argv[3])
	#seed = int(time.time())
	
	seed = 1369997168
	#8 10 1
	random.seed(seed)
	
	print("N = %d, M = %d, R = %d, seed = %d" % (N, M, R, seed))

	for r in range(R):

		D1 = [[INF for i in range(N)] for j in range(N)]
		D2 = [[INF for i in range(N)] for j in range(N)]

		# Set all diagonals to 0 and create a ring
		for i in range(N - 1):
			D1[i][i] = 0
			D2[i][i] = 0
			D1[i][i + 1] = 1
			D2[i][i + 1] = 1
		D1[N - 1][N - 1] = 0
		D2[N - 1][N - 1] = 0
		D1[N - 1][0] = 1
		D2[N - 1][0] = 1
		# then randomly place M 1s in each matrix
		m = N
		while m < M:
			i = random.randint(0, N - 1)
			j = random.randint(0, N - 1)
			if (i != j):
				if (D1[i][j] == INF):
					D1[i][j] = 1
					D2[i][j] = 1
					m = m + 1

		# solve using naive repeated squaring
		start = time.clock()
		naive(D1, N)
		elapsed1 = time.clock() - start

		for i in range(N):
			for j in range(N):
				print("%d " % (D1[i][j])),
			print

		print
		print("=================================")
		print

		start = time.clock()
		fast(D2, N)
		elapsed2 = time.clock() - start

		for i in range(N):
			for j in range(N):
				print("%d " % (D2[i][j])),
			print

		
		# Check the results
		for i in range(N):
			for j in range(N):
				if (D1[i][j] != D2[i][j]):
					print("!!! ERROR !!! D1[%d][%d] = %d, D2[%d][%d] = %d" % (i, j, D1[i][j], i, j, D2[i][j]))
		
		
		print("%f, %f" % (elapsed1, elapsed2))


if __name__ == "__main__":
	main()

