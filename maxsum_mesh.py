#!/usr/bin/python
import sys
import time
import random

INF = 2 ** 32


def serial(A, N):
	'''
	Serial algorithm for computing the maximum sub-array in the N-by-N 2D array 
	given by A. Runs in O(N^3) time.
	'''
	minSum = [[0 for j in range(0, N + 1)] for i in range(0, N + 1)]
	recSum = [[0 for j in range(0, N + 1)] for i in range(0, N + 1)]
	solSum = [[-INF for j in range(0, N + 1)] for i in range(0, N + 1)]
	colSum = [[0 for j in range(0, N + 1)] for i in range(0, N + 1)]

	SUM = -INF
	T = 0
	B = 0
	L = 0
	R = 0

	for k in range(1, N + 1):
		for i in range(1, N + 1):
			minSum[i][0] = 0
			recSum[i][0] = 0
			solSum[i][0] = -INF
		for j in range(1, N + 1):
			colSum[k - 1][j] = 0
		for i in range(k, N + 1):
			leftJ = 0
			rightJ = 0
			for j in range(1, N + 1):
				colSum[i][j] = colSum[i - 1][j] + A[i][j]
				recSum[i][j] = recSum[i][j - 1] + colSum[i][j]
				minSum[i][j] = min(recSum[i][j], minSum[i][j - 1])
				if minSum[i][j] < minSum[i][j - 1]:
					leftJ = j
				maxSum = recSum[i][j] - minSum[i][j]
				solSum[i][j] = max(maxSum, solSum[i][j - 1])
				if solSum[i][j] > solSum[i][j - 1]:
					rightJ = j
			if solSum[i][N] > SUM:
				SUM = solSum[i][N]
				T = k
				B = i
				L = leftJ
				R = rightJ

	#print("\nMaximum Sum: %d (T = %d, B = %d, L = %d, R = %d)" % (SUM, T, B, L + 1, R))
	return SUM


def mesh_2n(A, N):
	'''
	Simulation of a parallel distributed algorithm on an N-by-N mesh array.
	Takes 2N communication steps.
	'''
	colSum = [[0 for j in range(0, N + 1)] for i in range(0, N + 1)]
	minSum = [[0 for j in range(0, N + 1)] for i in range(0, N + 1)]
	recSum = [[0 for j in range(0, N + 1)] for i in range(0, N + 1)]
	control = [[False for j in range(0, N + 1)] for i in range(0, N + 1)]
	solSum = [[-INF for j in range(0, N + 1)] for i in range(0, N + 1)]

	colSumFromT = [[0 for j in range(0, N + 1)] for i in range(0, N + 1)]
	recSumFromL = [[0 for j in range(0, N + 1)] for i in range(0, N + 1)]
	minSumFromL = [[0 for j in range(0, N + 1)] for i in range(0, N + 1)]
	solSumFromT = [[-INF for j in range(0, N + 1)] for i in range(0, N + 1)]
	solSumFromL = [[-INF for j in range(0, N + 1)] for i in range(0, N + 1)]
	controlFromL = [[False for j in range(0, N + 1)] for i in range(0, N + 1)]

	for i in range(0, N + 1):
		control[i][0] = True

	for k in range(1, 2 * N):

		# Simulating data transfer in parallel
		for i in range(1, N + 1):
			for j in range(1, N + 1):
				colSumFromT[i][j] = colSum[i - 1][j]
				recSumFromL[i][j] = recSum[i][j - 1]
				minSumFromL[i][j] = minSum[i][j - 1]
				solSumFromT[i][j] = solSum[i - 1][j]
				solSumFromL[i][j] = solSum[i][j - 1]
				controlFromL[i][j] = control[i][j - 1]

		# Simulating computation in each cell in parallel
		for i in range(1, N + 1):
			for j in range(1, N + 1):
				if controlFromL[i][j]:
					colSum[i][j] = colSumFromT[i][j] + A[i][j]
					recSum[i][j] = recSumFromL[i][j] + colSum[i][j]
					minSum[i][j] = min(recSum[i][j], minSumFromL[i][j])
					maxSum = recSum[i][j] - minSum[i][j]
					solSum[i][j] = max(solSum[i][j], maxSum, solSumFromT[i][j], solSumFromL[i][j])
					control[i][j] = True

	return solSum[N][N]


def mesh_15n(A, N):
	'''
	Simulation of a parallel distributed algorithm on an N-by-N mesh array.
	Takes 1.5N communication steps.
	'''
	midSum = [0 for i in range(N + 2)]
	
	colSum = [[0 for j in range(N + 2)] for i in range(N + 2)]
	minSum = [[0 for j in range(N + 2)] for i in range(N + 2)]
	recSum = [[0 for j in range(N + 2)] for i in range(N + 2)]
	control = [[False for j in range(N + 2)] for i in range(N + 2)]
	maxSum = [[-INF for j in range(N + 2)] for i in range(N + 2)]
	solSum = [[-INF for j in range(N + 2)] for i in range(N + 2)]

	colSumFromT = [[0 for j in range(N + 2)] for i in range(N + 2)]
	recSumFromL = [[0 for j in range(N + 2)] for i in range(N + 2)]
	minSumFromL = [[0 for j in range(N + 2)] for i in range(N + 2)]
	recSumFromR = [[0 for j in range(N + 2)] for i in range(N + 2)]
	minSumFromR = [[0 for j in range(N + 2)] for i in range(N + 2)]
	solSumFromT = [[-INF for j in range(N + 2)] for i in range(N + 2)]
	solSumFromL = [[-INF for j in range(N + 2)] for i in range(N + 2)]
	solSumFromR = [[-INF for j in range(N + 2)] for i in range(N + 2)]
	controlFromL = [[False for j in range(N + 2)] for i in range(N + 2)]
	controlFromR = [[False for j in range(N + 2)] for i in range(N + 2)]

	for i in range(N + 1):
		control[i][0] = True
		control[i][N + 1] = True

	for k in range(1, (3 * N)/2):

		# Simulating data transfer in parallel
		for i in range(1, N + 1):
			for j in range(1, N + 1):
				colSumFromT[i][j] = colSum[i - 1][j]
				solSumFromT[i][j] = solSum[i - 1][j]
				if j <= N/2:
					recSumFromL[i][j] = recSum[i][j - 1]
					minSumFromL[i][j] = minSum[i][j - 1]
					solSumFromL[i][j] = solSum[i][j - 1]
					controlFromL[i][j] = control[i][j - 1]
				else:
					recSumFromR[i][j] = recSum[i][j + 1]
					minSumFromR[i][j] = minSum[i][j + 1]
					solSumFromR[i][j] = solSum[i][j + 1]
					controlFromR[i][j] = control[i][j + 1]

		# Simulating computation in each cell in parallel
		for i in range(1, N + 1):
			for j in range(1, N + 1):
				if j <= N/2 and controlFromL[i][j]:
					colSum[i][j] = colSumFromT[i][j] + A[i][j]
					recSum[i][j] = recSumFromL[i][j] + colSum[i][j]
					minSum[i][j] = min(recSum[i][j], minSumFromL[i][j])
					maxSum[i][j] = recSum[i][j] - minSum[i][j]
					solSum[i][j] = max(solSum[i][j], maxSum[i][j], solSumFromT[i][j], solSumFromL[i][j])
					control[i][j] = True
				elif j > N/2 and controlFromR[i][j]:
					colSum[i][j] = colSumFromT[i][j] + A[i][j]
					recSum[i][j] = recSumFromR[i][j] + colSum[i][j]
					minSum[i][j] = min(recSum[i][j], minSumFromR[i][j])
					maxSum[i][j] = recSum[i][j] - minSum[i][j]
					solSum[i][j] = max(solSum[i][j], maxSum[i][j], solSumFromT[i][j], solSumFromR[i][j])
					control[i][j] = True
			midSum[i] = max(midSum[i], midSum[i - 1], (maxSum[i][N/2] + maxSum[i][N/2 + 1]))

	#print("L = %d, M = %d, R = %d" % (solSum[N][N/2], midSum[N], solSum[N][N/2 + 1]))
	return max(solSum[N][N/2], solSum[N][N/2 + 1], midSum[N])


def mesh_1n(A, N):
	'''
	Simulation of a parallel distributed algorithm on an N-by-N mesh array.
	Takes 1N communication steps.
	'''

	colSumT = [[0 for j in range(N + 2)] for i in range(N + 2)]
	colSumB = [[0 for j in range(N + 2)] for i in range(N + 2)]
	
	recSumT = [[0 for j in range(N + 2)] for i in range(N + 2)]
	recSumB = [[0 for j in range(N + 2)] for i in range(N + 2)]
	recSumBPrev = [[0 for j in range(N + 2)] for i in range(N + 2)]
	
	recSumUE = [[0 for j in range(N + 2)] for i in range(N + 2)]
	recSumUO = [[0 for j in range(N + 2)] for i in range(N + 2)]

	recSumBPrev = [[0 for j in range(N + 2)] for i in range(N + 2)]

	minSumE = [[0 for j in range(N + 2)] for i in range(N + 2)]
	minSumO = [[0 for j in range(N + 2)] for i in range(N + 2)]
	maxSumE = [[-INF for j in range(N + 2)] for i in range(N + 2)]
	maxSumO = [[-INF for j in range(N + 2)] for i in range(N + 2)]

	control = [[False for j in range(N + 2)] for i in range(N + 2)]

	colSumTFromT = [[0 for j in range(N + 2)] for i in range(N + 2)]
	colSumBFromB = [[0 for j in range(N + 2)] for i in range(N + 2)]
	recSumTFromL = [[0 for j in range(N + 2)] for i in range(N + 2)]
	recSumTFromR = [[0 for j in range(N + 2)] for i in range(N + 2)]
	recSumBFromL = [[0 for j in range(N + 2)] for i in range(N + 2)]
	recSumBFromR = [[0 for j in range(N + 2)] for i in range(N + 2)]
	recSumBFromB = [[0 for j in range(N + 2)] for i in range(N + 2)]

	recSumBPrevFromB = [[0 for j in range(N + 2)] for i in range(N + 2)]

	minSumEFromL = [[0 for j in range(N + 2)] for i in range(N + 2)]
	minSumOFromL = [[0 for j in range(N + 2)] for i in range(N + 2)]
	minSumEFromR = [[0 for j in range(N + 2)] for i in range(N + 2)]
	minSumOFromR = [[0 for j in range(N + 2)] for i in range(N + 2)]

	controlFromL = [[False for j in range(N + 2)] for i in range(N + 2)]
	controlFromR = [[False for j in range(N + 2)] for i in range(N + 2)]

	maxSum = [[-INF for j in range(N + 2)] for i in range(N + 2)]
	solSum = [[-INF for j in range(N + 2)] for i in range(N + 2)]

	midSum = [0 for i in range(N + 2)]

	solSumFromT = [[-INF for j in range(N + 2)] for i in range(N + 2)]
	solSumFromB = [[-INF for j in range(N + 2)] for i in range(N + 2)]
	solSumFromL = [[-INF for j in range(N + 2)] for i in range(N + 2)]
	solSumFromR = [[-INF for j in range(N + 2)] for i in range(N + 2)]
	rowSumFromL = [[0 for j in range(N + 2)] for i in range(N + 2)]
	rowSumFromR = [[0 for j in range(N + 2)] for i in range(N + 2)]

	maxSumEFromR = [[-INF for j in range(N + 2)] for i in range(N + 2)]
	maxSumOFromR = [[-INF for j in range(N + 2)] for i in range(N + 2)]
	midSumFromT = [0 for i in range(N + 2)]
	midSumFromB = [0 for i in range(N + 2)]

	for i in range(N + 1):
		control[i][0] = True
		control[i][N + 1] = True

	for k in range(1, N + 1):

		# Simulating the first set of data transfer in parallel
		for i in range(1, N + 1):
			for j in range(1, N + 1):
				colSumTFromT[i][j] = colSumT[i - 1][j]
				solSumFromT[i][j] = solSum[i - 1][j]
				colSumBFromB[i][j] = colSumB[i + 1][j]
				solSumFromB[i][j] = solSum[i + 1][j]
				if j <= N/2:
					controlFromL[i][j] = control[i][j - 1]
					recSumTFromL[i][j] = recSumT[i][j - 1]
					recSumBFromL[i][j] = recSumB[i][j - 1]
				else:
					controlFromR[i][j] = control[i][j + 1]
					recSumTFromR[i][j] = recSumT[i][j + 1]
					recSumBFromR[i][j] = recSumB[i][j + 1]

		# Simulating the first set of computation in each cell in parallel
		for i in range(1, N + 1):
			for j in range(1, N + 1):
				if j <= N/2 and controlFromL[i][j]:
					colSumT[i][j] = colSumTFromT[i][j] + A[i][j]
					recSumT[i][j] = recSumTFromL[i][j] + colSumT[i][j]
					colSumB[i][j] = colSumBFromB[i][j] + A[i][j]
					recSumBPrev[i][j] = recSumB[i][j]
					recSumB[i][j] = recSumBFromL[i][j] + colSumB[i][j]
				elif j > N/2 and controlFromR[i][j]:
					colSumT[i][j] = colSumTFromT[i][j] + A[i][j]
					recSumT[i][j] = recSumTFromR[i][j] + colSumT[i][j]
					colSumB[i][j] = colSumBFromB[i][j] + A[i][j]
					recSumBPrev[i][j] = recSumB[i][j]
					recSumB[i][j] = recSumBFromR[i][j] + colSumB[i][j]

		# Simulating the second set of data transfer
		for i in range(1, N + 1):
			for j in range(1, N + 1):
				recSumBFromB[i][j] = recSumB[i + 1][j]
				recSumBPrevFromB[i][j] = recSumBPrev[i + 1][j]
				if j <= N/2:
					minSumEFromL[i][j] = minSumE[i][j - 1]
					minSumOFromL[i][j] = minSumO[i][j - 1]
					solSumFromL[i][j] = solSum[i][j - 1]
				else:
					minSumEFromR[i][j] = minSumE[i][j + 1]
					minSumOFromR[i][j] = minSumO[i][j + 1]
					solSumFromR[i][j] = solSum[i][j + 1]
		
		# Simulating the second set of computation
		for i in range(1, N + 1):
			for j in range(1, N + 1):
				if j <= N/2 and controlFromL[i][j]:
					recSumUE[i][j] = recSumT[i][j] + recSumBFromB[i][j]
					recSumUO[i][j] = recSumT[i][j] + recSumBPrevFromB[i][j]
					minSumE[i][j] = min(minSumEFromL[i][j], recSumUE[i][j])
					minSumO[i][j] = min(minSumOFromL[i][j], recSumUO[i][j])
					maxSumE[i][j] = recSumUE[i][j] - minSumE[i][j]
					maxSumO[i][j] = recSumUO[i][j] - minSumO[i][j]
					solSum[i][j] = max(solSum[i][j], solSumFromT[i][j], solSumFromB[i][j], solSumFromL[i][j], maxSumE[i][j], maxSumO[i][j])
					control[i][j] = True
				elif j > N/2 and controlFromR[i][j]:
					recSumUE[i][j] = recSumT[i][j] + recSumBFromB[i][j]
					recSumUO[i][j] = recSumT[i][j] + recSumBPrevFromB[i][j]
					minSumE[i][j] = min(minSumEFromR[i][j], recSumUE[i][j])
					minSumO[i][j] = min(minSumOFromR[i][j], recSumUO[i][j])
					maxSumE[i][j] = recSumUE[i][j] - minSumE[i][j]
					maxSumO[i][j] = recSumUO[i][j] - minSumO[i][j]
					solSum[i][j] = max(solSum[i][j], solSumFromT[i][j], solSumFromB[i][j], solSumFromR[i][j], maxSumE[i][j], maxSumO[i][j])
					control[i][j] = True

		# Simulating the third set of data transfer in parallel
		for i in range(1, N + 1):
			for j in range(1, N + 1):
				if j == N/2:
					maxSumEFromR[i][j] = maxSumE[i][j + 1]
					maxSumOFromR[i][j] = maxSumO[i][j + 1]
					midSumFromT[i] = midSum[i - 1]
					midSumFromB[i] = midSum[i + 1]

		# Simulating the third set of computations in each cell in parallel
		for i in range(1, N + 1):
			for j in range(1, N + 1):
				if j == N/2:
					midSumE = maxSumE[i][j] + maxSumEFromR[i][j]
					midSumO = maxSumO[i][j] + maxSumOFromR[i][j]
					#print("midSumE: %d, midSumO: %d\n" % (midSumE, midSumO))
					midSum[i] = max(midSum[i], midSumE, midSumO, midSumFromT[i], midSumFromB[i])
	
		#for i in range(1, N + 1):
		#	print("midSum[%d] = %d" % (i, midSum[i]))
	
	#print("S1 = %d, S2 = %d, M1 = %d, M2 = %d" % (solSum[N/2][N/2], solSum[N/2][N/2 + 1], midSum[N/2], midSum[N/2 + 1]))
	
	return max(solSum[N/2][N/2], solSum[N/2][N/2 + 1], midSum[N/2], midSum[N/2 + 1])


def main():

	if (len(sys.argv) != 3):
		print("Usage: %s <N> <R>" % sys.argv[0])
		return

	N = int(sys.argv[1])
	R = int(sys.argv[2])

	if (N % 2) != 0:
		print("The value given for N must be divisible by 2")
		return

	for r in range(R):
		print("======== Run %d" % (r))

		seed = int(time.time())
		#seed = 1400126597
		random.seed(seed)

		A = [[random.randint(-N, N) for j in range(N + 2)] for i in range(N + 2)]
		for i in range(N + 1):
			A[i][0] = -INF
			A[i][N + 1] = -INF
			A[0][i] = -INF
			A[N + 1][i] = -INF

		# if serial(A, N) != mesh_2n(A, N):
		# if serial(A, N) != mesh_15n(A, N):
		if serial(A, N) != mesh_1n(A, N):
			print("N = %d, seed = %d" % (N, seed))
			#for i in range(1, N + 1):
			#	for j in range(1, N + 1):
			#		print("%d\t" % (A[i][j])),
			#	print
			print('!!! Mesh array simulation failed !!!')
			sys.exit(0)

	print("Mesh array simulation was successful")

if __name__ == "__main__":
	main()


