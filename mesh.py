#!/usr/bin/python
import sys
import random

INF = 9999
MAX = 9

WRAP = False

DIRECTIONS = ['L', 'R', 'U', 'D']

class Cell:
	def __init__(self, p, q, R):
		self.p = p
		self.q = q
		self.dist = [[INF for t in range(R)] for s in range(R)]
		self.dFrom = {}
		self.sFrom = {}
		self.dSave = {}
		self.sSave = {}
		for direction in DIRECTIONS:
			self.dFrom[direction] = None
			self.sFrom[direction] = False
			self.dSave[direction] = None
			self.sSave[direction] = False


def L_and_U(cell, R):
	for k in range(R):
		for i in range(R):
			for j in range(R):
				cell.dist[i][j] = min(cell.dist[i][j], cell.dSave['L'][i][k] + cell.dSave['U'][k][j])

def U_and_R(cell, R):
	for k in range(R):
		for i in range(R):
			for j in range(R):
				cell.dist[i][j] = min(cell.dist[i][j], cell.dSave['R'][i][k] + cell.dSave['U'][k][j])

def R_and_D(cell, R):
	for k in range(R):
		for i in range(R):
			for j in range(R):
				cell.dist[i][j] = min(cell.dist[i][j], cell.dSave['R'][i][k] + cell.dSave['D'][k][j])

def D_and_L(cell, R):
	for k in range(R):
		for i in range(R):
			for j in range(R):
				cell.dist[i][j] = min(cell.dist[i][j], cell.dSave['L'][i][k] + cell.dSave['D'][k][j])

def just_L(cell, R):
	for k in range(R):
		for i in range(R):
			if (i != k):
				for j in range(R):
					cell.dist[i][j] = min(cell.dist[i][j], cell.dSave['L'][i][k] + cell.dist[k][j])

def just_U(cell, R):
	for k in range(R):
		for i in range(R):
			for j in range(R):
				if (j != k):
					cell.dist[i][j] = min(cell.dist[i][j], cell.dSave['U'][k][j] + cell.dist[i][k])

def just_R(cell, R):
	for k in range(R):
		for i in range(R):
			if (i != k):
				for j in range(R):
					cell.dist[i][j] = min(cell.dist[i][j], cell.dSave['R'][i][k] + cell.dist[k][j])

def just_D(cell, R):
	for k in range(R):
		for i in range(R):
			for j in range(R):
				if (j != k):
					cell.dist[i][j] = min(cell.dist[i][j], cell.dSave['D'][k][j] + cell.dist[i][k])

def compute_cell(cell, R):
	for k in range(R):
		for i in range(R):
			for j in range(R):
				cell.dist[i][j] = min(cell.dist[i][j], cell.dist[i][k] + cell.dist[k][j])


def floyd(A, N):
	for k in range(N):
		for i in range(N):
			for j in range(N):
				A[i][j] = min(A[i][j], A[i][k] + A[k][j])


def main():

	if (len(sys.argv) != 3):
		print("Usage: %s <n> <r>" % (sys.argv[0]))
		return

	N = int(sys.argv[1])
	R = int(sys.argv[2])

	if (N % R) != 0:
		print("N must be cleanly divisible by R")
		return

	S = N / R
	D = [[INF for j in range(N)] for i in range(N)]

	# Initialize diagonals to 0
	for i in range(N):
		D[i][i] = 0
	
	# Create a ring
	for i in range(N - 1):
		D[i][i + 1] = MAX
	D[N - 1][0] = MAX

	# Generate some random distances
	num = N
	while num < (R * N):
		i = random.randint(0, N - 1)
		j = random.randint(0, N - 1)
		if (i != j) and (D[i][j] == INF):
			num = num + 1
			D[i][j] = random.randint(1, MAX)

	A1 = [[D[i][j] for j in range(N)] for i in range(N)]
	A2 = [[D[i][j] for j in range(N)] for i in range(N)]

	# A1 = Solved with Floyd
	floyd(A1, N)

	# Each C[i][j] contains R * R sub-matrix.
	C = [[Cell(p, q, R) for q in range(S)] for p in range(S)]
	for i in range(N):
		for j in range(N):
			p = i / R
			q = j / R
			s = i % R
			t = j % R
			C[p][q].dist[s][t] = D[i][j]


	origin = 0
	for step in range((5 * S) - 4):

		for i in range(N):
			for q in range(S):
				p = i / R
				s = i % R
				print(C[p][q].dist[s]),
			print

		# Originate signals from origin in all four directions
		if (step % 3 == 0) and origin < S:
			cell = C[origin][origin]
			compute_cell(cell, R)
			for direction in DIRECTIONS:
				cell.sFrom[direction] = True
				cell.dFrom[direction] = list(cell.dist)
			origin = origin + 1
		
		# Save distances and signals from neighbours
		for p in range(S):
			for q in range(S):
				cell = C[p][q]
				for direction in DIRECTIONS:
					if cell.dFrom[direction] is None:
						cell.dSave[direction] = None
					else:
						cell.dSave[direction] = list(cell.dFrom[direction])
					cell.dFrom[direction] = None
					if cell.sFrom[direction]:
						cell.sSave[direction] = True
					else:
						cell.sSave[direction] = False
					cell.sFrom[direction] = False

		# Compute distances based on distance data from neighbours
		for p in range(S):
			for q in range(S):
				cell = C[p][q]
				if (cell.dSave['L'] is not None) and (cell.dSave['U'] is not None):
					L_and_U(cell, R)
				if (cell.dSave['U'] is not None) and (cell.dSave['R'] is not None):
					U_and_R(cell, R)
				if (cell.dSave['R'] is not None) and (cell.dSave['D'] is not None):
					R_and_D(cell, R)
				if (cell.dSave['D'] is not None) and (cell.dSave['L'] is not None):
					D_and_L(cell, R)

				if (cell.dSave['L'] is not None) and (cell.dSave['U'] is None) and (cell.dSave['D'] is None):
					just_L(cell, R)
				if (cell.dSave['U'] is not None) and (cell.dSave['L'] is None) and (cell.dSave['R'] is None):
					just_U(cell, R)
				if (cell.dSave['R'] is not None) and (cell.dSave['U'] is None) and (cell.dSave['D'] is None):
					just_R(cell, R)
				if (cell.dSave['D'] is not None) and (cell.dSave['L'] is None) and (cell.dSave['R'] is None):
					just_D(cell, R)
		
		# Forward data and signals
		for p in range(S):
			for q in range(S):
				cell = C[p][q]
				pPrev = p - 1
				qPrev = q - 1
				pNext = p + 1
				qNext = q + 1

				if WRAP:
					if pPrev < 0:
						pPrev = S - 1
					if qPrev < 0:
						qPrev = S - 1
					if pNext == S:
						pNext = 0
					if qNext == S:
						qNext = 0

				if cell.sSave['L']:
					if pNext < S:
						C[pNext][q].dFrom['U'] = list(cell.dist) # Copy data down
					if pPrev >= 0:
						C[pPrev][q].dFrom['D'] = list(cell.dist) # Copy data up
					if qNext < S:
						C[p][qNext].sFrom['L'] = True # Forward signal to the right

				if cell.sSave['U']:
					if qNext < S:
						C[p][qNext].dFrom['L'] = list(cell.dist) # Copy data to the right
					if qPrev >= 0:
						C[p][qPrev].dFrom['R'] = list(cell.dist) # Copy data to the left
					if pNext < S:
						C[pNext][q].sFrom['U'] = True # Forward signal down

				if cell.sSave['R']:
					if pNext < S:
						C[pNext][q].dFrom['U'] = list(cell.dist) # Copy data down
					if pPrev >= 0:
						C[pPrev][q].dFrom['D'] = list(cell.dist) # Copy data up
					if qPrev >= 0:
						C[p][qPrev].sFrom['R'] = True # Forward signal to the left

				if cell.sSave['D']:
					if qNext < S:
						C[p][qNext].dFrom['L'] = list(cell.dist) # Copy data to the right
					if qPrev >= 0:
						C[p][qPrev].dFrom['R'] = list(cell.dist) # Copy data to the left
					if pPrev >= 0:
						C[pPrev][q].sFrom['D'] = True # Forward signal up

				if cell.dSave['L'] is not None and qNext < S:
					C[p][qNext].dFrom['L'] = list(cell.dSave['L']) # Forward data to the right
				if cell.dSave['U'] is not None and pNext < S:
					C[pNext][q].dFrom['U'] = list(cell.dSave['U']) # Forward data down
				if cell.dSave['R'] is not None and qPrev >= 0:
					C[p][qPrev].dFrom['R'] = list(cell.dSave['R']) # Forward data to the left
				if cell.dSave['D'] is not None and pPrev >= 0:
					C[pPrev][q].dFrom['D'] = list(cell.dSave['D']) # Forward data up

		raw_input()

	for i in range(N):
		for q in range(S):
			p = i / R
			s = i % R
			print(C[p][q].dist[s]),
		print

	# Retreive A2 from C
	for i in range(N):
		for j in range(N):
			p = i / R
			q = j / R
			s = i % R
			t = j % R
			A2[i][j] = C[p][q].dist[s][t]

	for i in range(N):
		for j in range(N):
			if A1[i][j] != A2[i][j]:
				print("Incorrect result A1[%d][%d] = %d, A2[%d][%d] = %d" % (i, j, A1[i][j], i, j, A2[i][j]))
				return

if __name__ == "__main__":
	main()



