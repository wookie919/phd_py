#!/usr/bin/python
import sys
import time
import random
import math


def naive(A, C, N):
	l = 1
	while l < N:
		l = l * 2
		for i in range(N):
			for j in range(N):
				if (i != j):
					oldDist = C[i][j]
					for k in range(N):
						if (k != i) and (k != j):
							if (A[i][k] != 0) and (A[k][j] != 0):
								newDist = A[i][k] + A[k][j]
								if (oldDist == 0) or (oldDist > newDist):
									oldDist = newDist
					C[i][j] = oldDist

		for i in range(N):
			for j in range(N):
				A[i][j] = C[i][j]


def new(A, C, N):
	l = 1
	while l < N:
		l = int(math.ceil(l * 1.5))
		lmin = int(math.ceil(l / 2.0))

		S = [[] for i in range(N)]
		U = [[] for i in range(N)]
		for i in range(N):
			O = [0 for d in range(N)]
			for j in range(N):
				d = A[i][j]
				if (d >= lmin) and (d <= l):
					O[d] = O[d] + 1
			occ = N
			dist = 0
			lmax = l + 1
			if lmax > N:
				lmax = N
			for d in range(lmin, lmax):
				if (O[d] > 0) and (O[d] < occ):
					occ = O[d]
					dist = d
			for j in range(N):
				if A[i][j] == dist:
					S[i].append(j)
				elif A[i][j] == 0:
					U[i].append(j)

		for i in range(N):
			#for j in range(N):
			for j in U[i]:
				if (i != j):
					oldDist = C[i][j]
					for k in S[i]:
						if (k != i) and (k != j):
							if (A[i][k] > 0) and (A[k][j] > 0):
								newDist = A[i][k] + A[k][j]
								if (newDist <= l):
									if (oldDist == 0) or (oldDist > newDist):
										oldDist = newDist
					C[i][j] = oldDist

		for i in range(N):
			for j in range(N):
				A[i][j] = C[i][j]



def BMM(C, W, A, B, N, debug=False):
	'''
	C = A * B
	W = witness
	N = matrix size
	'''
	for i in range(N):
		for j in range(N):
			if (i != j):
				for k in range(N):
					if (i != k) and (k != j):
						if (A[i][k] > 0) and (B[k][j] > 0):
							C[i][j] = 1
							if W[i][j] < 0:
								W[i][j] = k
								if debug:
									print("i = %d, j = %d, k = %d" % (i, j, k))
							break

def DMM(C, W, A, B, N, debug=False):
	'''
	C = A * B
	W = witness
	N = matrix size
	'''
	for i in range(N):
		for j in range(N):
			if (i != j):
				for k in range(N):
					if (i != k) and (k != j):
						minimum = C[i][j]
						witness = -1
						if (A[i][k] > 0) and (B[k][j] > 0):
							if (minimum == 0) or (minimum > A[i][k] + B[k][j]):
								minimum = A[i][k] + B[k][j]
								W[i][j] = k
								if debug:
									print("i = %d, j = %d, k = %d" % (i, j, k))
						C[i][j] = minimum


def fast(B1, D, N):

	t = int(math.ceil(N ** 0.2))
	#t = 3

	Bt = [[0 for i in range(N)] for j in range(N)]
	Bs = [[0 for i in range(N)] for j in range(N)]
	M = [[0 for i in range(N)] for j in range(N)]
	R = [[0 for i in range(N)] for j in range(N)]
	
	print("Acceleration phase")
	
	for l in range(2, t + 1):

		R = [[0 for i in range(N)] for j in range(N)]
		for i in range(N):
			for j in range(N):
				if D[i][j] > 0:
					R[i][j] = 1
				else:
					R[i][j] = 0

		C = [[0 for i in range(N)] for j in range(N)]
		Wt = [[0 for i in range(N)] for j in range(N)]
		BMM(C, Wt, R, B1, N)

		for i in range(N):
			for j in range(N):
				if (D[i][j] == 0) and (C[i][j] > 0):
					D[i][j] = l

	print("=====")
	for i in range(N):
		for j in range(N):
			print("%d " % (D[i][j])),
		print

	Bt = [[0 for i in range(N)] for j in range(N)]
	for i in range(N):
		for j in range(N):
			if (i == j):
				Bt[i][j] = 1
				Bs[i][j] = 1
			elif (D[i][j] > 0):
				Bt[i][j] = 1
				Bs[i][j] = 1
			else:
				Bt[i][j] = 0
				Bs[i][j] = 0
		

	print("Boosting phase, l = t = %d" % t)
	l = t

	C = [[0 for i in range(N)] for j in range(N)]
	W = [[-1 for i in range(N)] for j in range(N)]
	E = [[0 for i in range(N)] for j in range(N)]
	BMM(C, W, Bt, B1, N)
	for i in range(N):
		for j in range(N):
			if (C[i][j] == 1) and (Bt[i][j] == 0) and (D[i][j] == 0):
				D[i][j] = t + 1
				E[i][j] = t + 1

	r = t * t
	X = [[0 for i in range(N)] for j in range(N)]
	for s in range(1, r):
		Bs1 = [[0 for i in range(N)] for j in range(N)]
		BMM(Bs1, W, Bs, Bt, N)
		for i in range(N):
			for j in range(N):
				if (Bs1[i][j] == 1) and (Bs[i][j] == 0):
					X[i][j] = (s + 1) * t
		C = [[0 for i in range(N)] for j in range(N)]
		BMM(C, W, Bs1, B1, N)
		for i in range(N):
			for j in range(N):
				if (C[i][j] == 1) and (Bs1[i][j] == 0) and (D[i][j] == 0):
					D[i][j] = ((s + 1) * t) + 1
					E[i][j] = ((s + 1) * t) + 1
		for i in range(N):
			for j in range(N):
				Bs[i][j] = Bs1[i][j]

	print("--------- D ---------")
	for i in range(N):
		for j in range(N):
			print("%d " % (D[i][j])),
		print
	print("--------- E ---------")
	for i in range(N):
		for j in range(N):
			print("%d " % (E[i][j])),
		print

	for i in range(N):
		for j in range(N):
			E[i][j] = E[i][j] / t

	print("--------- E ---------")
	for i in range(N):
		for j in range(N):
			print("%d " % (E[i][j])),
		print

	for s in range(2, t + 1):
		F = [[0 for i in range(N)] for j in range(N)]
		DMM(F, W, E, B1, N)

		'''
		print("--------- F ---------")
		for i in range(N):
			for j in range(N):
				print("%d " % (F[i][j])),
			print
		'''
		
		for i in range(N):
			for j in range(N):
				if (F[i][j] > 0) and (E[i][j] == 0):
					F[i][j] = F[i][j] - 1
					if ((F[i][j] * t) + s <= X[i][j]) and (D[i][j] == 0):
						D[i][j] = (F[i][j] * t) + s
					else:
						F[i][j] = 0
				else:
					F[i][j] = 0
		for i in range(N):
			for j in range(N):
				E[i][j] = F[i][j]
		'''
		print("--------- D ---------")
		for i in range(N):
			for j in range(N):
				print("%d " % (D[i][j])),
			print
		print("--------- E ---------")
		for i in range(N):
			for j in range(N):
				print("%d " % (E[i][j])),
			print
		'''

	l = t * t

	print("Cruising phase, l = t^2 = %d" % l)
	while l < N:

		R = [[0 for i in range(N)] for j in range(N)]
		for i in range(N):
			for j in range(N):
				if D[i][j] > 0:
					R[i][j] = 1
				else:
					R[i][j] = 0

		lmin = int(math.ceil(l / 2.0))
		lnew = int(math.ceil(l * 1.5))

		# Determine bridging set Si for each row
		S = [[] for i in range(N)]
		for i in range(N):
			O = [0 for d in range(N)]
			for j in range(N):
				d = D[i][j]
				if (d >= lmin) and (d <= l):
					O[d] = O[d] + 1
			occ = N
			dist = 0
			lmax = l + 1
			if lmax > N:
				lmax = N
			for d in range(lmin, lmax):
				if (O[d] > 0) and (O[d] < occ):
					occ = O[d]
					dist = d
			for j in range(N):
				if D[i][j] == dist:
					#print("S[%d]: %d" % (i,j))
					S[i].append(j)

		DT = [[0 for i in range(N)] for j in range(N)]
		for i in range(N):
			for j in range(N):
				if i != j:
					oldDist = D[i][j]
					for k in S[i]:
						if (k != i) and (k != j):
							if (D[i][k] > 0) and (D[k][j] > 0):
								newDist = D[i][k] + D[k][j]
								if (newDist <= lnew):
									if (oldDist == 0) or (oldDist > newDist):
										oldDist = newDist
					DT[i][j] = oldDist

		for i in range(N):
			for j in range(N):
				D[i][j] = DT[i][j]

		l = lnew

		'''
		for i in range(N):
			for j in range(N):
				print("%d " % (D[i][j])),
			print
		'''


def main():

	if (len(sys.argv) < 4):
		print("Usage: %s <N> <M> <R>" % sys.argv[0])
		return

	N = int(sys.argv[1])
	M = int(sys.argv[2])
	R = int(sys.argv[3])
	seed = int(time.time())
	
	#seed = 1369712647
	random.seed(seed)
	
	print("N = %d, M = %d, R = %d, seed = %d" % (N, M, R, seed))

	for r in range(R):
	
		A1 = [[0 for i in range(N)] for j in range(N)]
		A2 = [[0 for i in range(N)] for j in range(N)]
		C1 = [[0 for i in range(N)] for j in range(N)]
		C2 = [[0 for i in range(N)] for j in range(N)]

		# First create a ring
		for i in range(N - 1):
			A1[i][i + 1] = 1
			A2[i][i + 1] = 1
			C1[i][i + 1] = 1
			C2[i][i + 1] = 1
		A1[N - 1][0] = 1
		A2[N - 1][0] = 1
		C1[N - 1][0] = 1
		C2[N - 1][0] = 1
		# then randomly place M 1s in each matrix
		m = N
		while m < M:
			i = random.randint(0, N - 1)
			j = random.randint(0, N - 1)
			if (i != j):
				if (A1[i][j] == 0):
					A1[i][j] = 1
					A2[i][j] = 1
					C1[i][j] = 1
					C2[i][j] = 1
					m = m + 1

		# solve using naive repeated squaring
		start = time.clock()
		naive(A1, C1, N)
		elapsed1 = time.clock() - start

		# solve using the new method
		#start = time.clock()
		#new(A1, C1, N)
		#elapsed1 = time.clock() - start

		start = time.clock()
		fast(A2, C2, N)
		elapsed2 = time.clock() - start

		# Check the results
		for i in range(N):
			for j in range(N):
				if (C1[i][j] != C2[i][j]):
					print("!!! ERROR !!! C1[%d][%d] = %d, C2[%d][%d] = %d" % (i, j, C1[i][j], i, j, C2[i][j]))

		print("%f, %f" % (elapsed1, elapsed2))


if __name__ == "__main__":
	main()
