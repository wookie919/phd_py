#!/usr/bin/python
import sys
import time
import random
import math

def GCD(m, n):
	if m < n:
		r = n
		n = m
		m = r
	while n != 0:
		r = m % n
		m = n
		n = r
	return m


def simple(A, B, C, N):
	for i in range(N):
		for j in range(N):
			for k in range(N):
				if (A[i][k] > 0 and B[k][j] > 0):
					C[i][j] = 1
					break


def new(A, B, C, N):

	P = [0 for i in range(N)]
	P[0] = 2
	P[1] = 3
	p = 2
	c = 3
	while p < N:
		c = c + 2
		sq = int(math.sqrt(c)) + 1
		i = 1
		prime = True
		while P[i] <= sq and i < p:
			if (c % P[i] == 0):
				prime = False
				break
			i = i + 1
		if prime:
			P[p] = c
			p = p + 1

	t = 1
	for p in range(16):
		t = t * P[p]
	print(t)
	print(2**64 - 1)
			
	RP = [1 for i in range(N)]
	CP = [1 for i in range(N)]

	for i in range(N):
		for j in range(N):
			if (A[i][j] > 0):
				A[i][j] = P[j]
				RP[i] = RP[i] * P[j]

	for i in range(N):
		for j in range(N):
			if (B[i][j] > 0):
				B[i][j] = P[i]
				CP[j] = CP[j] * P[i]

	for i in range(N):
		for j in range(N):
			if (GCD(RP[i], CP[j]) > 1):
				C[i][j] = 1


def main():

	if (len(sys.argv) < 3):
		print("Usage: %s <N> <M>" % sys.argv[0])
		return

	N = int(sys.argv[1])
	M = int(sys.argv[2])
	seed = int(time.time())
	random.seed(seed)
	print("N = %d, M = %d, seed = %d" % (N, M, seed))
		
	A = [[0 for i in range(N)] for j in range(N)]
	B = [[0 for i in range(N)] for j in range(N)]
	C1 = [[0 for i in range(N)] for j in range(N)]
	C2 = [[0 for i in range(N)] for j in range(N)]

	# Randomly place M 1s in each matrix
	if M >= N * N:
		A = [[1 for i in range(N)] for j in range(N)]
		B = [[1 for i in range(N)] for j in range(N)]
	m = 0
	while m < M:
		i = random.randint(0, N - 1)
		j = random.randint(0, N - 1)
		if (A[i][j] == 0):
			A[i][j] = 1
			m = m + 1
	m = 0
	while m < M:
		i = random.randint(0, N - 1)
		j = random.randint(0, N - 1)
		if (B[i][j] == 0):
			B[i][j] = 1
			m = m + 1

	# Multiply using the simple method
	start = time.clock()
	simple(A, B, C1, N)
	elapsed1 = time.clock() - start

	# Multiply using the new method
	start = time.clock()
	new(A, B, C2, N)
	elapsed2 = time.clock() - start

	# Check the results
	for i in range(N):
		for j in range(N):
			if (C1[i][j] != C2[i][j]):
				print("ERROR!!!")

	print("%f, %f" % (elapsed1, elapsed2))

	
if __name__ == "__main__":
	main()
