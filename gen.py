#!/usr/bin/python

N = 8

fp = open('mesh.html', 'w')

fp.write('<html>\n')
fp.write('<head>\n')
fp.write('<meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>')
fp.write('<title>Mesh Floyd - 8 x 8 </title>\n')
fp.write('<link href="style.css" rel="stylesheet" type="text/css"/>\n')
fp.write('<script src="jscript.js"></script>\n')
fp.write('</head>\n')

fp.write('<body>\n')

fp.write('<table class="matrix">\n')
for i in range(N):
	fp.write('<tr>')
	for j in range(N):
		fp.write('<td id="%d%d" class="cell">' % (i, j))
		fp.write('<table class="matrix">')
		fp.write('<tr><td id="%d%d_ul" class="corner"></td><td id="%d%d_uc" class="edge"></td><td id="%d%d_ur" class="corner"></td></tr>' % (i, j, i, j, i, j))
		fp.write('<tr><td id="%d%d_ml" class="edge"></td><td id="%d%d_mc" class="center"></td><td id="%d%d_mr" class="edge"></td></tr>' % (i, j, i, j, i, j))
		fp.write('<tr><td id="%d%d_ll" class="corner"></td><td id="%d%d_lc" class="edge"></td><td id="%d%d_lr" class="corner"></td></tr>' % (i, j, i, j, i, j))
		fp.write('</table>')
		fp.write('</td>')
	fp.write('</tr>\n')
fp.write('</table>\n')
	
fp.write('<button type="button" onclick="generate(\'%d\')">Generate new matrix</button>\n' % (N))
fp.write('<button type="button" onclick="prev()">&lt;&lt;</button>\n')
fp.write('<span id="cycle">-</span>\n')
fp.write('<button type="button" onclick="next()">&gt;&gt;</button>\n')

fp.write('</body>\n')
fp.write('<html>\n')
fp.close()
