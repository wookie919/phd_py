#!/usr/bin/python
import sys
import time
import math
import random


def naive(C, A, B, n, m, l):
	"""
	Perform naive DMM (A = D1 x D2) in O(n^3) time.
	"""
	for i in range(m):
		for j in range(m):
			curMin = n
			for k in range(l):
				if A[i][k] + B[k][j] < curMin:
					curMin = A[i][k] + B[k][j]
					viaNode = k
			C[i][j] = curMin
			#print("C[%d][%d] via node: %d" % (i, j, viaNode))


class Diff:
	def __init__(self, i, r, s, diff):
		self.i = i
		self.r = r
		self.s = s
		self.diff = diff
		self.rank = -1


def Takaoka92(C, A, B, n, m, l, I):
	"""
	Perform DMM (C = A x B) in O(n^3 (loglogn/logn)^(1/2)) time.
	"""
	#m = int(math.log(N, 2) / math.log(math.log(N, 2), 2))
	#l = int(math.sqrt(m))

	(H, L) = diff_rank_encode(A, B, m, l)

	for i in range(m):
		for j in range(m):
			k = I[H[i]][L[j]]
			C[i][j] = A[i][k] + B[k][j]
			#print("C[%d][%d] via node: %d" % (i, j, viaNode))


def diff_rank_encode(A, B, m, l):
	H = [[[None for i in range(m)] for s in range(l)] for r in range(l)]
	L = [[[None for i in range(m)] for s in range(l)] for r in range(l)]
	for r in range(l - 1):
		for s in range(r + 1, l):
			for i in range(m):
				H[r][s][i] = Diff(i, r, s, A[i][r] - A[i][s])
				L[r][s][i] = Diff(i, r, s, B[s][i] - B[r][i])
	
	G = [[[] for s in range(l)] for r in range(l)]
	for r in range(l - 1):
		for s in range(r + 1, l):
			for i in range(m):
				G[r][s].append(H[r][s][i])
				G[r][s].append(L[r][s][i])
			G[r][s].sort(key = lambda x: x.diff)
			rank = 0
			for diff in G[r][s]:
				diff.rank = rank
				rank = rank + 1

	H_enc = [0 for i in range(m)]
	L_enc = [0 for i in range(m)]

	mu = 2 * m
	k = (l * (l - 1)) / 2

	for i in range(m):
		maxMu = mu ** (k - 1)
		encH = 0
		encL = 0
		for r in range(l - 1):
			for s in range(r + 1, l):
				encH = encH + (maxMu * H[r][s][i].rank)
				encL = encL + (maxMu * L[r][s][i].rank)
				maxMu = maxMu / mu
		H_enc[i] = encH
		L_enc[i] = encL

	return (H_enc, L_enc)


def compute_lookup_table(m, l):
	mu = 2 * m
	k = (l * (l - 1)) / 2
	
	maxIdx = 0
	base = 1
	for i in range(k):
		maxIdx = maxIdx + ((mu - 1) * base)
		base = base * mu
	I = [[0 for j in range(maxIdx)] for i in range(maxIdx)]

	for i in range(maxIdx):
		H = decode(i, m, l)
		for j in range(maxIdx):
			L = decode(j, m, l)
			I[i][j] = computeK(H, L, l)

	return I


def decode(num, m, l):
	mu = 2 * m
	k = (l * (l - 1)) / 2

	decodedMatrix = [[0 for j in range(l + 1)] for i in range(l + 1)]
	rankList = [0 for i in range(k)]
	
	for i in range(k):
		rankList[i] = num % mu
		num = int(num / mu)
	rankList.reverse()

	idx = 0
	for i in range(l - 1):
		for j in range(i + 1, l):
			decodedMatrix[i][j] = rankList[idx]
			idx = idx + 1

	return decodedMatrix


def computeK(H, L, l):
	for k in range(l):
		isK = True
		for s in range(k + 1, l):
			if H[k][s] >= L[k][s]:
				isK = False
				break
		for r in range(k):
			if H[r][k] <= L[r][k]:
				isK = False
				break
		if isK:
			return k
	return -1


def main():

	if (len(sys.argv) < 5):
		print("Usage: %s <n> <m> <l> <r>" % (sys.argv[0]))
		return

	n = int(sys.argv[1])
	m = int(sys.argv[2])
	l = int(sys.argv[3])
	r = int(sys.argv[4])
	seed = int(time.time())
	#seed = 1369712647
	random.seed(seed)
	print("n = %d, m = %d, l = %d, r = %d, seed = %d" % (n, m, l, r, seed))
	
	######################################
	# Vertex numbers are from 0 to n - 1 #
	######################################
	
	start = time.clock()
	I = compute_lookup_table(m, l)
	elapsed = time.clock() - start
	print("Time taken to compute the lookup table: %f" % (elapsed))
	
	for runNum in range(r):
		#D1 = [[random.random() for j in range(n)] for i in range(n)]
		#D2 = [[random.random() for j in range(n)] for i in range(n)]

		A = [[random.random() for j in range(l)] for i in range(m)]
		B = [[random.random() for j in range(m)] for i in range(l)]

		# the number of vertices can be used as INFINITY
		# because all distances are between 0 and 1.
		C1 = [[n for j in range(m)] for i in range(m)]
		C2 = [[n for j in range(m)] for i in range(m)]

		# solve using naive method
		start = time.clock()
		naive(C1, A, B, n, m, l)
		elapsed1 = time.clock() - start

		# solve using Takaoka92
		start = time.clock()
		Takaoka92(C2, A, B, n, m, l, I)
		elapsed2 = time.clock() - start

		# check the results
		for i in range(m):
			for j in range(m):
				if (C1[i][j] != C2[i][j]):
					print("!!! ERROR !!! C1[%d][%d] = %f, C2[%d][%d] = %f" % (i, j, A1[i][j], i, j, A2[i][j]))

		print("%f, %f" % (elapsed1, elapsed2))


if __name__ == "__main__":
	main()

