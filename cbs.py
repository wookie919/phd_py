#!/usr/bin/python
import sys
import time
import random
import math

INF = 2 ** 32


class Bucket:
	def __init__(self, numBuckets):
		self.buckets = [None for i in range(numBuckets)]
		self.numItems = 0
		self.numBuckets = numBuckets
		self.minKey = INF
	
	def add(self, item, key):
		if self.buckets[key] is not None:
			self.buckets[key].prev = item
			item.next = self.buckets[key]
		self.buckets[key] = item
		self.numItems = self.numItems + 1
		if key < self.minKey:
			self.minKey = key

	def pop(self):
		if self.numItems == 0:
			return None
		for key in range(self.minKey, self.numBuckets):
			if self.buckets[key] is not None:
				item = self.buckets[key]
				self.buckets[key] = item.next
				item.next = None
				item.prev = None
				if self.buckets[key] is not None:
					self.buckets[key].prev = None
				self.numItems = self.numItems - 1
				return item

	def update(self, item, curKey, newKey):
		if item.prev is not None:
			item.prev.next = item.next
		else:
			self.buckets[curKey] = item.next
		if item.next is not None:
			item.next.prev = item.prev
		item.next = None
		item.prev = None
		self.add(item, newKey)





class CBS:
	def __init__(self, n, c):
		# 0 index will not be used because it's too confusing.
		self.p = 10
		self.k = int(math.ceil(math.log((c/n), self.p)) + 1)
		self.cbs = [None for level in range(0, self.k + 1)]
		self.a = [0 for level in range(0, self.k + 1)]
		self.b = [0 for level in range(0, self.k + 1)]
		self.pk = self.p ** (self.k - 1)
		self.maxA = ((c * n) / self.pk) + 1
		
		for i in range(0, self.k):
			self.cbs[i] = [None for bucket in range(0, self.p)]
		self.cbs[self.k] = [None for bucket in range(0, self.maxA)]

		self.numItems = 0

		
		
		
		
		
	def add(self, item, key):
		level = self.k
		div = self.pk
		while True:
			if (key / div) > 0:
				bucket = key / div
				break
			div = div / self.p
			level = level - 1
		print("Add: (%d,%d), %d, level = %d, bucket = %d" % (item.src, item.dst, key, level, bucket))

		if self.cbs[level][bucket] is not None:
			self.cbs[level][bucket].prev = item
			item.next = self.cbs[level][bucket]
		self.cbs[level][bucket] = item
		item.level = level
		item.bucket = bucket
		self.numItems = self.numItems + 1

		#print("ADD: items in queue = %d" % (self.numItems))

		
		
		
		
		
	def pop(self):

		#print("POP: num items in queue = %d" % (self.numItems))
		#self.dump()
		
		itemToReturn = None
	
		if self.numItems == 0:
			return itemToReturn

		self.numItems = self.numItems - 1
		
		#print("POP: self.a[1] = %d" % (self.a[1]))
		
		while (self.cbs[1][self.a[1]] is None):
			self.a[1] = self.a[1] + 1
			if (self.a[1] == self.p):
				break
		
		if self.a[1] < self.p:
			bucket = self.a[1]
			#print("Pop: found an item on level 1 in bucket %d" % (bucket))
			item = self.cbs[1][bucket]
			self.cbs[1][bucket] = item.next
			item.next = None
			item.prev = None
			item.level = 0
			item.bucket = 0
			if self.cbs[1][bucket] is not None:
				self.cbs[1][bucket].prev = None
			itemToReturn = item
		else:
			#print("Pop: need to perform the CASCADE operation")
			level = 2
			found = False
			
			# find the lowest level/bucket that is non-emtpy.
			while True:
				if level < self.k:
					maxA = self.p
				else:
					maxA = self.maxA
				#print("level = %d, maxA = %d" % (level, maxA))
				while (self.a[level] < maxA):
					if self.cbs[level][self.a[level]] is not None:
						#print("Pop: found non-empty bucket at level %d, bucket %d" % (level, self.a[level]))
						found = True
						break
					self.a[level] = self.a[level] + 1
				if found:
					break
				level = level  + 1
			
			# cascade down to level 1, update self.b while cascading down.
			while level > 1:
				nextLevel = level - 1
				if level < self.k:
					self.b[level] = self.b[level + 1] + (self.a[level] * (self.p ** (level - 1)))
				bucket = self.a[level]
				
				#print("self.a[%d] is %d" % (level, self.a[level]))
				#print("self.b[%d] updated to %d" % (level, self.b[level]))
				
				while self.cbs[level][bucket] is not None:
					# detach the item
					item = self.cbs[level][bucket]
					self.cbs[level][bucket] = item.next
					item.next = None
					item.prev = None
					if self.cbs[level][bucket] is not None:
						self.cbs[level][bucket].prev = None
					#print("Pop: (%d,%d), %d detached" % (item.src, item.dst, item.cost))
					
					# find new location
					div = self.p ** (level - 1)
					key = (item.cost % div) / (div / self.p)
					
					# insert item in new location
					if self.cbs[nextLevel][key] is not None:
						self.cbs[nextLevel][key].prev = item
						item.next = self.cbs[nextLevel][key]
					self.cbs[nextLevel][key] = item
					item.level = nextLevel
					item.bucket = key
					#print("Pop: (%d,%d), %d inserted into level %d, bucket %d" % (item.src, item.dst, item.cost, item.level, item.bucket))
					
					# update self.a accordingly
					if self.a[nextLevel] > key:
						self.a[nextLevel] = key
					
					#print("Pop: self.a[%d] updated to %d" % (nextLevel, self.a[nextLevel]))
					
				level = level - 1

			# return item on level 1, bucket self.a[1]
			item = self.cbs[1][self.a[1]]
			self.cbs[1][self.a[1]] = item.next
			item.next = None
			item.prev = None
			if self.cbs[1][self.a[1]] is not None:
				self.cbs[1][self.a[1]].prev = None
			itemToReturn = item
		
		#print("Pop: (%d,%d), %d popped" % (item.src, item.dst, item.cost))
		return itemToReturn

		
		
		
		

	def update(self, item, curKey, newKey):
		
		curLevel = item.level
		curBucket = item.bucket
		
		# detach the item
		if item.prev is not None:
			item.prev.next = item.next
		else:
			self.cbs[curLevel][curBucket] = item.next
		if item.next is not None:
			item.next.prev = item.prev
		item.next = None
		item.prev = None
		
		# find the new location based on self.b
		#print("UPDATE: curKey = %d, newKey = %d, curLevel = %d, curBucket = %d" % (curKey, newKey, curLevel, curBucket))
		
		#print("UPDATE: self.a[%d] = %d" % (curLevel, self.a[curLevel]))
		
		newPosFound = False
		if curLevel == self.k:
			if newKey >= ((self.a[curLevel] * self.pk) + self.pk):
				newLevel = curLevel
				newBucket = newKey / self.pk
				newPosFound = True
		
		if not newPosFound:
			while curLevel > 1:
				#print("UPDATE: self.b[%d] = %d" % (curLevel, self.b[curLevel]))
				if (newKey % self.pk) >= (self.b[curLevel] + (self.p ** (curLevel - 1))):
					newLevel = curLevel
					newBucket = (newKey % (self.p ** (curLevel + 1))) / (self.p ** curLevel)
					break
				curLevel = curLevel - 1

		# insert the item
		if self.cbs[newLevel][newBucket] is not None:
			self.cbs[newLevel][newBucket].prev = item
			item.next = self.cbs[newLevel][newBucket]
		self.cbs[newLevel][newBucket] = item
		item.level = newLevel
		item.bucket = newBucket

		
		
		
		
		
	def dump(self):
		for level in range(1, self.k):
			for bucket in range(1, self.p):
				if self.cbs[level][bucket] is not None:
					print("level = %d, bucket = %d is not empty" % (level, bucket))
		for bucket in range(1, self.maxA):
			if self.cbs[self.k][bucket] is not None:
				print("level = %d, bucket = %d is not empty" % (self.k, bucket))

		
		
		



class Edge:
	def __init__(self, src, dst, cost):
		self.src = src
		self.dst = dst
		self.cost = cost
		self.prev = None
		self.next = None
		self.level = 0
		self.bucket = 0





def main():
	if (len(sys.argv) != 4):
		print("Usage: %s <n> <m> <c>" % (sys.argv[0]))
		return

	N = int(sys.argv[1])
	M = int(sys.argv[2])
	C = int(sys.argv[3])
	seed = int(time.time())
	#seed = 1389584687
	random.seed(seed)
	print("n = %d, m = %d, c = %d, seed = %d" % (N, M, C, seed))

	D = [[INF for j in range(N)] for i in range(N)]
	Edges = [[None for j in range(N)] for i in range(N)]

	# Generate random edge costs
	for i in range(N - 1):
		D[i][i + 1] = C
		Edges[i][i + 1] = Edge(i, i + 1, C)
	D[N - 1][0] = C
	Edges[N - 1][0] = Edge(N - 1, 0, C)
	for i in range(N):
		D[i][i] = 0
	numEdges = N
	while numEdges < M:
		src = random.randrange(N)
		dst = random.randrange(N)
		if (src != dst) and (Edges[src][dst] is None):
			numEdges = numEdges + 1
			cost = random.randint(1, C)
			D[src][dst] = cost
			Edges[src][dst] = Edge(src, dst, cost)

	# Solve using Floyd
	for k in range(N):
		for i in range(N):
			for j in range(N):
				D[i][j] = min(D[i][j], D[i][k] + D[k][j])
	
	# Solve using Pairwise method
	queue = Bucket(C * N + 1)
	#queue = CBS(N, C)
	for src in range(N):
		for dst in range(N):
			edge = Edges[src][dst]
			if edge is not None:
				queue.add(edge, edge.cost)

	print
	print("===========================================")
	print

	while True:
		edge = queue.pop()
		if edge is None:
			break
		u = edge.src
		v = edge.dst
		for w in range(N):
			if (v != w) and (u != w) and (Edges[v][w] is not None):
				# u -> v -> w
				newCost = edge.cost + Edges[v][w].cost
				if (Edges[u][w] is not None):
					if (newCost < Edges[u][w].cost):
						oldCost = Edges[u][w].cost
						Edges[u][w].cost = newCost
						queue.update(Edges[u][w], oldCost, newCost)
				else:
					Edges[u][w] = Edge(u, w, newCost)
					queue.add(Edges[u][w], newCost)


	'''
	for i in range(N):
		for j in range(N):
			edge = Edges[i][j]
			if edge is None:
				print("0 "),
			else:
				print("%d " % (edge.cost)),
		print
	print
	for i in range(N):
		for j in range(N):
			print("%d " % (D[i][j])),
		print
	'''

	for i in range(N):
		for j in range(N):
			if (i != j) and (D[i][j] != Edges[i][j].cost):
				print("%d -> %d: %d != %d" % (i, j, D[i][j], Edges[i][j].cost)),
				return


if __name__ == "__main__":
	main()
