#!/usr/bin/python
import sys
import time
import random

INF = 2 ** 32

def main():

	if (len(sys.argv) < 2):
		print("Usage: %s <N>" % sys.argv[0])
		return

	N = int(sys.argv[1])
	seed = int(time.time())
	random.seed(seed)
	print("N = %d, seed = %d" % (N, seed))
	
	A = [[random.randint(-N, N) for j in range(0, N + 1)] for i in range(0, N + 1)]

	for i in range(1, N + 1):
		for j in range(1, N + 1):
			print(A[i][j]),
		print

	minSum = [[INF for j in range(0, N + 1)] for i in range(0, N + 1)]
	curSum = [[0 for j in range(0, N + 1)] for i in range(0, N + 1)]
	solSum = [[-INF for j in range(0, N + 1)] for i in range(0, N + 1)]

	SUM = -INF
	
	T = 0
	B = 0
	L = 0
	R = 0
	
	# TO DO: not efficient because the same sums are calculated over and over again.
	# *Should* be straightforward to optimize by preprocessing all colSums and rowSums.
	
	for k in range(1, N + 1):
		colSum = [[0 for j in range(0, N + 1)] for i in range(0, N + 1)]
		for i in range(k, N + 1):
			for j in range(1, N + 1):
				colSum[i][j] = colSum[i - 1][j] + A[i][j]
				curSum[i][j] = curSum[i][j - 1] + colSum[i][j]
				minSum[i][j] = min(curSum[i][j], minSum[i][j - 1])
				if minSum[i][j] < minSum[i][j - 1]:
					leftJ = j
				maxSum = curSum[i][j] - minSum[i][j]
				solSum[i][j] = max(maxSum, solSum[i][j - 1])
				if solSum[i][j] > solSum[i][j - 1]:
					rightJ = j
			if solSum[i][N] > SUM:
				SUM = solSum[i][N]
				T = k
				B = i
				L = leftJ
				R = rightJ

	print("\nMaximum Sum: %d (T = %d, B = %d, L = %d, R = %d)" % (SUM, T, B, L + 1, R))

	print("\nMaximum Sub-array:")
	checkSum = 0
	for i in range(T, B + 1):
		for j in range(L + 1, R + 1):
			checkSum = checkSum + A[i][j]
			print(A[i][j]),
		print

	if checkSum != SUM:
		print('!!! ERROR !!! - calculated maximum sum is probably incorrect')


if __name__ == "__main__":
	main()

