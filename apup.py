#!/usr/bin/python
import sys
import os
import time
import random
import math

class Queue:
		def __init__(self):
			self.in_stack = []
			self.out_stack = []
		def push(self, obj):
			self.in_stack.append(obj)
		def pop(self):
			if not self.out_stack:
				while self.in_stack:
					self.out_stack.append(self.in_stack.pop())
			if not self.out_stack:
				return None
			else:
				return self.out_stack.pop()


def simple(NUM_NODES, NUM_EDGES, MAX_CAP, EDGE_CAPS, SRC, PATHS):

	class Edge:
		def __init__(self, src, dst, cap):
			self.src = src
			self.dst = dst
			self.cap = cap

	class Node:
		def __init__(self, id):
			self.id = id
			self.cap = 0
			self.parent = None
			self.edges = []
			self.inQ = False

	EDGES = [[None for dst in range (NUM_NODES)] for src in range(NUM_NODES)]
	for src in range(NUM_NODES):
		for dst in range(NUM_NODES):
			if EDGE_CAPS[src][dst] > 0:
				EDGES[src][dst] = Edge(src, dst, EDGE_CAPS[src][dst])

	NODES = [Node(n) for n in range(NUM_NODES)]
	for src in range(NUM_NODES):
		for dst in range(NUM_NODES):
			if not (EDGES[src][dst] is None):
				NODES[src].edges.append(EDGES[src][dst])

	EDGES_SORTED = []
	for src in range(NUM_NODES):
		for dst in range(NUM_NODES):
			if EDGES[src][dst] is not None:
				EDGES_SORTED.append(EDGES[src][dst])
	EDGES_SORTED.sort(key = lambda edge: edge.cap)

	Q = Queue()
	for cap in range(MAX_CAP + 1):

		for node in NODES:
			node.inQ = False
			node.parent = None

		NODES[SRC].inQ = True
		node = NODES[SRC]
		while not (node is None):
			for edge in node.edges:
				if (edge.cap >= cap):
					dst = edge.dst
					if not NODES[dst].inQ:
						NODES[dst].parent = node
						Q.push(NODES[dst])
						NODES[dst].inQ = True
			node = Q.pop()

		for node in NODES:
			dst = node.id
			if dst != SRC:
				dist = 0
				curNode = node.parent
				while curNode is not None:
					dist = dist + 1
					curNode = curNode.parent
				if PATHS[SRC][dst][dist] < cap and dist > 0:
					PATHS[SRC][dst][dist] = cap




def ssup(NUM_NODES, NUM_EDGES, MAX_CAP, EDGE_CAPS, SRC, PATHS):

	class Edge:
		def __init__(self, src, dst, cap):
			self.src = src
			self.dst = dst
			self.cap = cap

	class Node:
		def __init__(self, id):
			self.id = id
			self.cap = 0
			self.dist = 0
			self.parent = None
			self.edgesIn = []
			self.inTree = False

	EDGES = [[None for dst in range (NUM_NODES)] for src in range(NUM_NODES)]
	for src in range(NUM_NODES):
		for dst in range(NUM_NODES):
			if EDGE_CAPS[src][dst] > 0:
				EDGES[src][dst] = Edge(src, dst, EDGE_CAPS[src][dst])

	EDGES_SORTED = []
	for src in range(NUM_NODES):
		for dst in range(NUM_NODES):
			if EDGES[src][dst] is not None:
				EDGES_SORTED.append(EDGES[src][dst])
	EDGES_SORTED.sort(key = lambda edge: edge.cap)

	NODES = [Node(n) for n in range(NUM_NODES)]
	for edge in EDGES_SORTED:
		NODES[edge.dst].edgesIn.append(edge)

	Q = [Queue() for d in range(NUM_NODES)]

	srcNode = NODES[SRC]
	srcNode.cap = MAX_CAP
	srcNode.inTree = True

	for CAP in range(MAX_CAP + 1):

		# Remove low capacity nodes from the Tree and add them to Q
		for node in NODES:
			if node.cap < CAP:
				node.parent = None
				node.dist = node.dist + 1
				if node.dist < NUM_NODES:
					Q[node.dist].push(node)

		# Add all nodes in Q back to the Tree at the shortest distance possible
		for d in range(1, NUM_NODES):
			node = Q[d].pop()
			while node is not None:
				for edge in node.edgesIn:
					if edge.cap >= CAP:
						parent = NODES[edge.src]
						if (parent.dist == node.dist - 1):
							if edge.cap < parent.cap:
								minCap = edge.cap
							else:
								minCap = parent.cap
							if minCap > node.cap:
								node.parent = parent
								node.cap = minCap
				if node.parent is not None:
					PATHS[SRC][node.id][node.dist] = node.cap
				else:
					node.dist = node.dist + 1
					if node.dist < NUM_NODES:
						Q[node.dist].push(node)
				node = Q[d].pop()



def apup(NUM_NODES, NUM_EDGES, MAX_CAP, EDGE_CAPS, PATHS):

	E = [[0  for j in range(NUM_NODES)] for i in range(NUM_NODES)]
	C = [[0  for j in range(NUM_NODES)] for i in range(NUM_NODES)]
	D = [[[0 for c in range(MAX_CAP + 1)] for j in range(NUM_NODES)] for i in range(NUM_NODES)]
	L = [[[0 for j in range(NUM_NODES)] for i in range(NUM_NODES)] for c in range(MAX_CAP + 1)]

	for i in range(NUM_NODES):
		for j in range(NUM_NODES):
			c = EDGE_CAPS[i][j]
			if c > 0:
				E[i][j] = c
				C[i][j] = c
				D[i][j][c] = 1

	'''
	for i in range(NUM_NODES):
		for j in range(NUM_NODES):
			print("%d " % Ci[i][j]),
		print
	print
	'''

	#
	# Accelerating Phase O(rn^3)
	#
	r = int(math.sqrt(MAX_CAP))
	for l in range(2, r + 1):

		updateList = []
		for i in range(NUM_NODES):
			for j in range(NUM_NODES):
				if i != j:
					updated = False
					c = C[i][j]
					for k in range(NUM_NODES):
						if k != i and k != j:
							if C[i][k] < E[k][j]:
								cmin = C[i][k]
							else:
								cmin = E[k][j]
							if cmin > c:
								updated = True
								c = cmin
					if updated:
						updateList.append((i, j, c))
		for (i, j, c) in updateList:
			C[i][j] = c
			D[i][j][c] = l

		'''
		for i in range(NUM_NODES):
			for j in range(NUM_NODES):
				Co[i][j] = Cn[i][j]
				#print("%d " % Cn[i][j]),
			#print
		#print
		'''


	'''
	for i in range(NUM_NODES):
		for j in range(NUM_NODES):
			for c in range(1, MAX_CAP + 1):
				print("%d " % (D[i][j][c])),
			print("  "),
		print
	print
	'''

	#
	# Changing gears O(cn^2) Create L based on D
	#
	for i in range(NUM_NODES):
		for j in range(NUM_NODES):
			if i != j:
				for c in range(MAX_CAP, 0, -1):
					if D[i][j][c] > 0:
						break
				l = D[i][j][c]
				while c > 0:
					L[c][i][j] = l
					c = c - 1
					if D[i][j][c] > 0 and D[i][j][c] < l:
						l = D[i][j][c]

	'''
	for c in range(1, MAX_CAP + 1):
		print("c = %d" % (c))
		for i in range(NUM_NODES):
			for j in range(NUM_NODES):
				print("%d " % (L[c][i][j])),
			print
	'''

	#
	# Cruising Phase O(cn^3/r)
	#
	l = r
	while True:
		l = int(math.ceil(l * 1.5))

		lmin = int(math.ceil(l / 2.0))
		
		for c in range(1, MAX_CAP + 1):

			S = [[] for i in range(NUM_NODES)]
			for i in range(NUM_NODES):
				C = [0 for d in range(NUM_NODES)]
				maxJ = 0
				for j in range(NUM_NODES):
					d = L[c][i][j]
					if d >= lmin and d <= l:
						C[d] = C[d] + 1
						maxJ = j
				occ = NUM_NODES
				dist = 0
				lmax = l + 1
				if lmax > NUM_NODES:
					lmax = NUM_NODES
				for d in range(lmin, lmax):
					if C[d] > 0 and C[d] < occ:
						occ = C[d]
						dist = d
				for j in range(maxJ + 1):
					if L[c][i][j] == dist:
						S[i].append(j)

			updateList = []
			for i in range(NUM_NODES):
				for j in range(NUM_NODES):
					if i != j:
						updated = False
						d1 = L[c][i][j]
						for k in S[i]:
							if k != i and k != j:
								if L[c][i][k] > 0 and L[c][k][j] > 0:
									d2 = L[c][i][k] + L[c][k][j]
									if d2 <= l:
										if d1 > d2 or d1 == 0:
											updated = True
											d1 = d2
						if updated:
							updateList.append((i, j, d1))

			for (i, j, d) in updateList:
				L[c][i][j] = d

		'''
		for c in range(1, MAX_CAP + 1):
			print("c = %d" % (c))
			for i in range(NUM_NODES):
				for j in range(NUM_NODES):
					print("%d " % (L[c][i][j])),
				print
		'''

		if l > NUM_NODES:
			break


	# Just to compare results with other algorithms
	for c in range(1, MAX_CAP + 1):
		for i in range(NUM_NODES):
			for j in range(NUM_NODES):
				if i != j:
					if PATHS[i][j][L[c][i][j]] < c:
						PATHS[i][j][L[c][i][j]] = c



def main():

	if (len(sys.argv) < 5):
		print("Usage: %s <NUM_NODES> <NUM_EDGES> <MAX_CAP> <NUM_RUNS>" % sys.argv[0])
		return

	NUM_NODES = int(sys.argv[1])
	NUM_EDGES = int(sys.argv[2])
	MAX_CAP = int(sys.argv[3])
	NUM_RUNS = int(sys.argv[4])
	seed = int(time.time())
	#seed = 1364008443
	random.seed(seed)
	print("n = %d, m = %d, c = %d, r = %d, seed = %d" % (NUM_NODES, NUM_EDGES, MAX_CAP, NUM_RUNS, seed))

	EDGE_CAPS = [[0 for i in range(NUM_NODES)] for j in range(NUM_NODES)]

	# Start with a ring and generate some more random edges
	numEdges = NUM_NODES
	for i in range(NUM_NODES - 1):
		EDGE_CAPS[i][i + 1] = MAX_CAP
	EDGE_CAPS[NUM_NODES - 1][0] = MAX_CAP
	while numEdges < NUM_EDGES:
		src = random.randint(0, NUM_NODES - 1)
		dst = random.randint(0, NUM_NODES - 1)
		if (src != dst) and (EDGE_CAPS[src][dst] == 0):
			EDGE_CAPS[src][dst] = random.randint(1, MAX_CAP)
			numEdges = numEdges + 1

	'''
	for src in range(NUM_NODES):
		for dst in range(NUM_NODES):
			print("%.2f " % EDGE_CAPS[src][dst]),
		print
	'''

	PATH1 = [[[0 for dist in range(NUM_NODES)] for dst in range(NUM_NODES)] for src in range(NUM_NODES)]
	PATH2 = [[[0 for dist in range(NUM_NODES)] for dst in range(NUM_NODES)] for src in range(NUM_NODES)]
	PATH3 = [[[0 for dist in range(NUM_NODES)] for dst in range(NUM_NODES)] for src in range(NUM_NODES)]
	
	for run in range(NUM_RUNS):

		start = time.clock()
		for SRC in range(NUM_NODES):
			simple(NUM_NODES, NUM_EDGES, MAX_CAP, EDGE_CAPS, SRC, PATH1)
		elapsed = (time.clock() - start)
		print(elapsed)

		start = time.clock()
		for SRC in range(NUM_NODES):
			ssup(NUM_NODES, NUM_EDGES, MAX_CAP, EDGE_CAPS, SRC, PATH2)
		elapsed = (time.clock() - start)
		print(elapsed)

		start = time.clock()
		apup(NUM_NODES, NUM_EDGES, MAX_CAP, EDGE_CAPS, PATH3)
		elapsed = (time.clock() - start)
		print(elapsed)
		
		print("Comparing PATH1 and PATH2")
		for src in range(NUM_NODES):
			for dst in range(NUM_NODES):
				for d in range(NUM_NODES):
					if PATH1[src][dst][d] != PATH2[src][dst][d]:
						print("!!! ERROR !!!: %d %d %d" % (src, dst, d))
						print("%.2f %.2f" % (PATH1[src][dst][d], PATH2[src][dst][d]))


		print("Comparing PATH1 and PATH3")
		for src in range(NUM_NODES):
			for dst in range(NUM_NODES):
				for d in range(NUM_NODES):
					if PATH1[src][dst][d] != PATH3[src][dst][d]:
						print("!!! ERROR !!!: %d %d %d" % (src, dst, d))
						print("%.2f %.2f" % (PATH1[src][dst][d], PATH3[src][dst][d]))


if __name__ == "__main__":
	main()

